/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.pa165.dogbarbershop.dto;


/**
 *
 * @author Baly
 */
public class EmployeeDTO extends PersonDTO {

    private double salary;
    
    public EmployeeDTO() {}
    
    public EmployeeDTO(Long id, String firstName, String lastName, String email, AddressDTO address, String phone, double salary) {
        super(id, firstName, lastName, email, address, phone);
        this.salary = salary;
    }

    /**
     * @return the salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "EmployeeDTO [salary=" + salary + ", getId()=" + getId() + ", getFirstName()=" + getFirstName()
                + ", getLastName()=" + getLastName() + ", getEmail()=" + getEmail() + ", getAddress()=" + getAddress()
                + ", getPhone()=" + getPhone() + ", getFullName()=" + getFullName() + "]";
    }

}
