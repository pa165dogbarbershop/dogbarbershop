package cz.fi.muni.pa165.dogbarbershop.service;

import java.util.List;

import cz.fi.muni.pa165.dogbarbershop.dto.AccountDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.PersonDTO;


/**
 *
 * Services which handles requests for account.
 *
 * @author Jakub Knetl
 *
 */
public interface AccountService {

    /**
     * Create account
     *
     * @param accountDTO
     */
    public void create(AccountDTO accountDTO);

    /**
     * Update account
     *
     * @param accountDTO
     */
    public void update(AccountDTO accountDTO);

    /**
     * Delete account
     *
     * @param accountDTO
     */
    public void delete(AccountDTO accountDTO);

    /**
     * Get account with given id.
     *
     * @param id
     * @return Account with given id or null if it was not found
     */
    public AccountDTO find(Long id);
    
    /**
     * Get account with given login.
     *
     * @param login
     * @return Account with given login or null if it was not found
     */
    public AccountDTO find(String login);


    /**
     * Get all accounts
     * @return List of all accounts
     */
    public List<AccountDTO> findAll();

    /**
     * Return true if current user has the given role
     * 
     * @param role
     * @return true if has given role
     */
    public boolean hasRole(String role);
    
    /**
     * Return currently logged in account
     * 
     * @return account
     */
    public AccountDTO getLoggedAccount();

}
