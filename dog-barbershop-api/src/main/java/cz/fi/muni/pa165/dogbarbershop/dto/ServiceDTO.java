/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.pa165.dogbarbershop.dto;

import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author Baly
 */
@XmlRootElement
public class ServiceDTO {

    private Long id;
    
    private String name;

    private int duration;

    private double price;

    private PetDTO pet;
    
    public ServiceDTO() {}
    
    public ServiceDTO(Long id, String name, int duration, double price, PetDTO pet) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.price = price;
        this.pet = pet;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the pet
     */
    public PetDTO getPet() {
        return pet;
    }

    /**
     * @param pet the pet to set
     */
    public void setPet(PetDTO pet) {
        this.pet = pet;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ServiceDTO other = (ServiceDTO) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ServiceDTO [id=" + id + ", name=" + name + ", duration=" + duration + ", price=" + price + ", pet="
                + pet.getFullName() + "]";
    }

}
