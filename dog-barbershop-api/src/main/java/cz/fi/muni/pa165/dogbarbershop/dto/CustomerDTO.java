/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.pa165.dogbarbershop.dto;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Baly
 */
public class CustomerDTO extends PersonDTO {

    private int customerNum;

    private Set<PetDTO> pets;
    
    public CustomerDTO(Long id, String firstName, String lastName, String email, AddressDTO address, String phone, int customerNum, Set<PetDTO> pets) {
        super(id, firstName, lastName, email, address, phone);
        this.customerNum = customerNum;
        this.pets = pets;
    }
    
    public CustomerDTO(){}

    /**
     * @return the customerNum
     */
    public int getCustomerNum() {
        return customerNum;
    }

    /**
     * @param customerNum the customerNum to set
     */
    public void setCustomerNum(int customerNum) {
        this.customerNum = customerNum;
    }

    /**
     * @return the pets
     */
    public Set<PetDTO> getPets() {
        return pets;
    }

    /**
     * @param pets the pets to set
     */
    @JsonBackReference
    public void setPets(Set<PetDTO> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        return "CustomerDTO [customerNum=" + customerNum + ", pets=" + pets + ", getId()=" + getId()
                + ", getFirstName()=" + getFirstName() + ", getLastName()=" + getLastName() + ", getEmail()="
                + getEmail() + ", getAddress()=" + getAddress() + ", getPhone()=" + getPhone() + ", getFullName()="
                + getFullName() + "]";
    }

}
