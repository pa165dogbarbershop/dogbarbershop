/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.pa165.dogbarbershop.service;

import cz.fi.muni.pa165.dogbarbershop.dto.ServiceDTO;

import java.util.List;

/**
 *
 * @author Petr Baláš
 */
public interface ServiceService {
    /**
    * Create Service
    * 
    * @param serviceDTO
    */
   public void create(ServiceDTO serviceDTO);

   /**
    * Update Service
    * 
    * @param serviceDTO
    */
   public void update(ServiceDTO serviceDTO);

   /**
    * Delete Service
    * 
     * @param serviceDTO
    */
   public void delete(ServiceDTO serviceDTO);

   /**
    * Get service with given id.
    * 
    * @param id
    * @return Pet with given id or null if it was not found
    */
   public ServiceDTO find(Long id);

   /**
    * Get all Services
    * @return List of all Services
    */
   public List<ServiceDTO> findAll();
}
