/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.pa165.dogbarbershop.service;

import cz.fi.muni.pa165.dogbarbershop.dto.PetDTO;

import java.util.List;

/**
 *
 * @author Petr Baláš
 */
public interface PetService {
    /**
    * Create pet
    * 
    * @param petDTO
    */
   public void create(PetDTO petDTO);

   /**
    * Update pet
    * 
    * @param petDTO
    */
   public void update(PetDTO petDTO);

   /**
    * Delete pet
    * 
     * @param petDTO
    */
   public void delete(PetDTO petDTO);

   /**
    * Get pet with given id.
    * 
    * @param id
    * @return Pet with given id or null if it was not found
    */
   public PetDTO find(Long id);

   /**
    * Get all pets
    * @return List of all pets
    */
   public List<PetDTO> findAll();
}
