/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.pa165.dogbarbershop.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 *
 * @author Baly
 */
public class PetDTO {

    private Long id;

    private String name;

    private String breed;

    private Date birthDate; 

    private FurTypeDTO fur;

    private CustomerDTO customer = null;

    private List<ServiceDTO> services;
    
    public enum FurTypeDTO {
        
        SHORT,

        MEDIUM,

        LONG;
    }
    
    public PetDTO() {}
    
    public PetDTO(Long id, String name, String breed, Date birthDate, FurTypeDTO fur, CustomerDTO customer, List<ServiceDTO> services) {
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.birthDate = birthDate;
        this.fur = fur;
        this.customer = customer;
        this.services = services;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the breed
     */
    public String getBreed() {
        return breed;
    }
    
    /**
     * @param breed the breed to set
     */
    public void setBreed(String breed) {
        this.breed = breed;
    }

    /**
     * @return the birthDate
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDate the birthDate to set
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * @return the fur
     */
    public FurTypeDTO getFur() {
        return fur;
    }

    /**
     * @param fur the fur to set
     */
    public void setFur(FurTypeDTO fur) {
        this.fur = fur;
    }

    /**
     * @return the customer
     */
    public CustomerDTO getCustomer() {
        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    /**
     * @return the services
     */
    public List<ServiceDTO> getServices() {
        return services;
    }
    
    public String getFullName() {
        return customer.getFullName()+"'s "+getName();
    }

    /**
     * @param services the services to set
     */
    @JsonBackReference
    public void setServices(List<ServiceDTO> services) {
        this.services = services;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PetDTO other = (PetDTO) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PetDTO [id=" + id + ", name=" + name + ", breed=" + breed + ", birthDate=" + birthDate + ", fur=" + fur
                + ", customer=" + customer.getFullName() + ", services=" + services + "]";
    }

}
