/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.pa165.dogbarbershop.service;

import cz.fi.muni.pa165.dogbarbershop.dto.EmployeeDTO;

import java.util.List;

/**
 *
 * @author Petr Baláš
 */
public interface EmployeeService {

    /**
    * Create pet
    * 
    * @param employeeDTO
    */
    public void create(EmployeeDTO employeeDTO);

    /**
    * Update employee
    * 
    * @param employeeDTO
    */
    public void update(EmployeeDTO employeeDTO);

    /**
    * Delete employee
    * 
    * @param employeeDTO
    */
    public void delete(EmployeeDTO employeeDTO);

    /**
    * Get employee with given id.
    * 
    * @param id
    * @return Employee with given id or null if it was not found
    */
    public EmployeeDTO find(Long id);

    /**
    * Get all employees
    * @return List of all employees
    */
    public List<EmployeeDTO> findAll();
}
