package cz.fi.muni.pa165.dogbarbershop.dto;

/**
 * 
 * @author Jakub Knetl
 *
 */
public class AddressDTO {

	private String street;
	private String zip;
	private String city;

	public AddressDTO() {
	}

	public AddressDTO(String street, String zip, String city) {
		super();
		this.street = street;
		this.zip = zip;
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

    @Override
    public String toString() {
        return "AddressDTO [street=" + street + ", zip=" + zip + ", city=" + city + "]";
    }

}
