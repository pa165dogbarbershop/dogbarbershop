package cz.fi.muni.pa165.dogbarbershop.service;

import java.util.List;

import cz.fi.muni.pa165.dogbarbershop.dto.CustomerDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.PersonDTO;


/**
 * 
 * Services which handles requests for customer.
 * 
 * @author Jakub Knetl
 *
 */
public interface CustomerService {
	
	/**
	 * Create customer
	 * 
	 * @param customerDTO
	 */
	public void create(CustomerDTO customerDTO);
	
	/**
	 * Update customer
	 * 
	 * @param customerDTO
	 */
	public void update(CustomerDTO customerDTO);
	
	/**
	 * Delete customer
	 * 
	 * @param customerDTO
	 */
	public void delete(CustomerDTO customerDTO);
	
	/**
	 * Get customer with given id.
	 * 
	 * @param id
	 * @return Customer with given id or null if it was not found
	 */
	public CustomerDTO find(Long id);
	
	/**
	 * Get all customers
	 * @return List of all customers
	 */
	public List<CustomerDTO> findAll();
	
        public CustomerDTO personToCustomer(PersonDTO person);
}
