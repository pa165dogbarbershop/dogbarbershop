package cz.fi.muni.pa165.dogbarbershop.dao.test;

import cz.fi.muni.pa165.dogbarbershop.dao.PetDAO;
import cz.fi.muni.pa165.dogbarbershop.dao.ServiceDAO;
import cz.fi.muni.pa165.dogbarbershop.entity.Pet;
import cz.fi.muni.pa165.dogbarbershop.entity.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author David Balcak
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class ServiceDAOTest {

    @Autowired
    private ServiceDAO serviceDAO;
    
    @Autowired
    private PetDAO petDAO;
    
    @Autowired
    private JpaTransactionManager txManager;
    
    TransactionTemplate transactionTemplate;

    @Before
    public void setUp() throws Exception {
        transactionTemplate = new TransactionTemplate(txManager);
        
        transactionTemplate.execute(new TransactionCallback<Object>() {

            public Object doInTransaction(TransactionStatus ts) {
                List<Service> allS = serviceDAO.findAll();
                List<Pet> allP = petDAO.findAll();
                
                for(Service s : allS)
                    serviceDAO.delete(s);
                
                for(Pet p : allP)
                    petDAO.delete(p);
                
                return null;
            }
        });
    }
    
    @Test
    public void createService() {
        final Service s = newService();
        Assert.assertNull(s.getId());
        
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.create(s);
                
                return s;
            }
        });
        
        Assert.assertNotNull(s.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void createNullService() {
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.create(null);
                
                return null;
            }
        });
        
        Assert.fail("Created null Service");
    }
    
    @Test(expected = PersistenceException.class)
    public void createServiceWithId() {
        final Service s = newService();
        s.setId(1L);
        
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.create(s);
                
                return s;
            }
        });
        
        Assert.fail("Created Service with set Id");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void createExistingService() {
        final Service s = newService();
        
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.create(s);
                serviceDAO.create(s);
                
                return s;
            }
        });

        Assert.fail(s.toString() + " already in DB");
    }
    
    @Test(expected = PersistenceException.class)
    public void createServiceWithoutAttributes() {
        final Service s = newService();
        
        s.setName(null);
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.create(s);
                
                return s;
            }
        });
        Assert.fail(s.toString() + " passed without name");
    }
    
    @Test
    public void findService() {
        final Service s = newService();
        Assert.assertNull(serviceDAO.find(Long.MAX_VALUE));
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.create(s);
                
                return s;
            }
        });
        Assert.assertEquals(serviceDAO.find(s.getId()), s);
    }
    
    @Test
    public void findServiceWithIncorrectArgument() {
        try {
            serviceDAO.find(null);
            Assert.fail("Found Service with null id");
        } catch (IllegalArgumentException e) {}
        try {
            serviceDAO.find(Long.MIN_VALUE);
            Assert.fail("Found Service with negative id");
        } catch (IllegalArgumentException e) {}
    }
    
    @Test
    public void findNonExistentService() {
        Assert.assertNull(serviceDAO.find(Long.MAX_VALUE));
    }
    
    @Test
    public void updateService() {
        final Service s = newService();
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.create(s);
                
                return s;
            }
        });
        
        final Pet p = new Pet();
        p.setName("Happy");
        p.setBreed("Yorkshire Terrier");
        p.setBirthDate(new Date());
        p.setFur(Pet.FurType.MEDIUM);
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.create(p);
                
                return p;
            }
        });
        
        s.setName("Fur Coloring");
        s.setPrice(400.0);
        s.setPet(p);
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.update(s);
                
                return s;
            }
        });
        
        Service updated = serviceDAO.find(s.getId());
        Assert.assertEquals(updated, s);
        Assert.assertEquals("Fur Coloring", updated.getName());
        Assert.assertEquals(400.0, updated.getPrice(), 0.0002);
        Assert.assertEquals(updated.getPet(), p);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateNullService() {
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.update(null);
                
                return null;
            }
        });
        Assert.fail("Updated null Service");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateNonExistentService() {
        transactionTemplate.execute(new TransactionCallback<Service>() {
            Service s = newService();
            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.update(s);
                
                return s;
            }
        });
        Assert.fail("Updated non-existent Service");
    }
    
    @Test
    public void deleteService() {
        final Service s = newService();
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.create(s);
                
                return s;
            }
        });
        
        long id = s.getId();
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.delete(s);
                
                return s;
            }
        });
        Assert.assertNull(serviceDAO.find(id));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void deleteNullService() {
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.delete(null);
                
                return null;
            }
        });
        Assert.fail("Deleted null Service");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void deleteNonExistentService() {
        transactionTemplate.execute(new TransactionCallback<Service>() {
            Service s = newService();
            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.delete(s);
                
                return s;
            }
        });
        Assert.fail("Deleted non-existent Service");
    }
    
    @Test
    public void findAllEmployees() {
        Assert.assertTrue(serviceDAO.findAll().isEmpty());
        
        final Service s1 = newService();
        final Service s2 = newService();
        final Service s3 = newService();
        
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.create(s1);
                
                return s1;
            }
        });
        
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.create(s2);
                
                return s2;
            }
        });
        
        transactionTemplate.execute(new TransactionCallback<Service>() {

            public Service doInTransaction(TransactionStatus ts) {
                serviceDAO.create(s3);
                
                return s3;
            }
        });
        
        List<Service> actual = serviceDAO.findAll();
        List<Service> expected = new ArrayList<Service>(Arrays.asList(s1, s2, s3));
        Assert.assertEquals(actual.size(), expected.size());
        
        for (Service s: new ArrayList<Service>(expected)) {
            if (actual.contains(s)) {
                actual.remove(s);
                expected.remove(s);
            }
        }
        Boolean areEqual = actual.isEmpty() && expected.isEmpty();
        if (!areEqual) {
            Assert.fail("Got different list of services than expected");
        }
    }
    
    private Service newService() {
        Service s = new Service();
        s.setName("Fur Trim");
        s.setDuration(600);
        s.setPrice(200.0);
        
        return s;
    }
}
