package cz.fi.muni.pa165.dogbarbershop.service.test;

import cz.fi.muni.pa165.dogbarbershop.dao.PetDAO;
import cz.fi.muni.pa165.dogbarbershop.dto.PetDTO;
import cz.fi.muni.pa165.dogbarbershop.entity.Pet;
import cz.fi.muni.pa165.dogbarbershop.service.impl.PetServiceImpl;

import java.util.Arrays;
import java.util.Date;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.Spy;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

/**
 *
 * @author David Balcak
 */
@RunWith(MockitoJUnitRunner.class)
public class PetServiceTest {
    
    private PetServiceImpl petService;
    private PetDAO mockedPetDAO;
    private Pet pet1;
    private Pet pet2;
    private PetDTO pet1DTO;
    private PetDTO pet2DTO;
    
    @Spy
    private Mapper dozerMapper = new DozerBeanMapper();
    
    @Before
    public void setup() {
        petService = new PetServiceImpl();
        mockedPetDAO = mock(PetDAO.class);
        petService.setPetDAO(mockedPetDAO);
        petService.setMapper(dozerMapper);
        
        pet1 = new Pet();
        pet1.setName("Happy");
        pet1.setBreed("Yorkshire Terrier");
        pet1.setBirthDate(new Date());
        pet1.setFur(Pet.FurType.MEDIUM);
        
        pet1DTO = dozerMapper.map(pet1, PetDTO.class);
        
        pet2 = new Pet();
        pet2.setName("Magic");
        pet2.setBreed("German Shepherd");
        pet2.setBirthDate(new Date());
        pet2.setFur(Pet.FurType.LONG);
        
        pet2DTO = dozerMapper.map(pet2, PetDTO.class);
        
        // findPet
        when(mockedPetDAO.find(1L)).thenReturn(pet1);
        when(mockedPetDAO.find(2L)).thenReturn(pet2);
        
        // findAllPets
        when(mockedPetDAO.findAll()).thenReturn(Arrays.asList(pet1, pet2));
        
        // createPet
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Pet p = (Pet) args[0];
                pet1.setId(1L);
                return null;
            }
        }).when(mockedPetDAO).create(pet1);
        
        // updatePet
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Pet p = (Pet) args[0];
                pet2.setId(p.getId());
                pet2.setName(p.getName());
                pet2.setBreed(p.getBreed());
                pet2.setBirthDate(p.getBirthDate());
                pet2.setFur(p.getFur());
                return null;
            }
        }).when(mockedPetDAO).update(pet1);
        
        // deletePet
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Pet p = (Pet) args[0];
                pet1.setId(null);
                return null;
            }
        }).when(mockedPetDAO).delete(pet1);
    }
    
    @Test
    public void createPet() {
        petService.create(pet1DTO);
        assertNotNull(pet1.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void createNullPet() {
        petService.create(null);
        fail("Created null Pet");
    }
    
    @Test
    public void updatePet() {
        petService.update(pet1DTO);
        assertEquals(pet1, pet2);
        assertEquals(pet1.getName(), pet2.getName());
        assertEquals(pet1.getBreed(), pet2.getBreed());
        assertEquals(pet1.getBirthDate(), pet2.getBirthDate());
        assertEquals(pet1.getFur(), pet2.getFur());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateNullPet() {
        petService.update(null);
        fail("Updated null Pet");
    }
    
    @Test
    public void deletePet() {
        petService.delete(pet1DTO);
        assertNull(pet1.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void deleteNullPet() {
        petService.delete(null);
        fail("Deleted null Pet");
    }
    
    @Test
    public void findPet() {
        assertNotNull(petService.find(1L));
        assertEquals(pet1DTO, petService.find(1L));
        assertNotNull(petService.find(2L));
        assertEquals(pet2DTO, petService.find(2L));
    }
    
    @Test
    public void findAllPets() {
        assertNotNull(petService.findAll());
        assertEquals(Arrays.asList(pet1DTO, pet2DTO), petService.findAll());
    }
}
