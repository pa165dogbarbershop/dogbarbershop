package cz.fi.muni.pa165.dogbarbershop.dozer;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Test;

import cz.fi.muni.pa165.dogbarbershop.dto.CustomerDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.EmployeeDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.PetDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.ServiceDTO;
import cz.fi.muni.pa165.dogbarbershop.entity.Address;
import cz.fi.muni.pa165.dogbarbershop.entity.Customer;
import cz.fi.muni.pa165.dogbarbershop.entity.Employee;
import cz.fi.muni.pa165.dogbarbershop.entity.Pet;
import cz.fi.muni.pa165.dogbarbershop.entity.Pet.FurType;
import cz.fi.muni.pa165.dogbarbershop.entity.Service;

public class DozerMapperTest {

	private Mapper dozerMapper = new DozerBeanMapper();

	@Test
	public void CustomerMappingTest() {
		Customer customer = new Customer();
		Address address = new Address();
		address.setCity("Brno");
		address.setStreet("Jánská");
		address.setZip("123456");
		customer.setAddress(address);
		customer.setEmail("jan.novak@mail.com");
		customer.setId(1l);
		customer.setFirstName("Jan");
		customer.setLastName("Novák");
		customer.setPhone("123456789");

		Set<Pet> pets = new HashSet<>();
		Pet p1 = new Pet();
		Pet p2 = new Pet();
		p1.setId(3l);
		p2.setId(5l);
		pets.add(p1);
		pets.add(p2);
		customer.setPets(pets);
		CustomerDTO customerDTO = dozerMapper.map(customer, CustomerDTO.class);

		assertEquals(customerDTO.getId(), customer.getId());
		assertEquals(customerDTO.getFirstName(), customer.getFirstName());
		assertEquals(customerDTO.getAddress().getCity(), customer.getAddress()
				.getCity());
		assertEquals(customerDTO.getAddress().getStreet(), customer
				.getAddress().getStreet());
		assertEquals(customerDTO.getAddress().getZip(), customer.getAddress()
				.getZip());
		assertEquals(customerDTO.getEmail(), customer.getEmail());
		assertEquals(customerDTO.getLastName(), customer.getLastName());
		assertEquals(customerDTO.getPhone(), customer.getPhone());

		assertEquals(customerDTO.getPets().size(), customer.getPets().size());
	}

	@Test
	public void serviceMappingTest() {
		Pet pet = new Pet();
		pet.setId(9l);

		Service service = new Service();
		service.setId(43l);
		service.setName("name");
		service.setDuration(50003);
		service.setPrice(5000);
		service.setPet(pet);

		ServiceDTO serviceDTO = dozerMapper.map(service, ServiceDTO.class);

		assertEquals(serviceDTO.getId(), service.getId());
		assertEquals(serviceDTO.getName(), service.getName());
		assertEquals(serviceDTO.getDuration(), service.getDuration());
		assertEquals(serviceDTO.getPrice(), service.getPrice(), 0.0001);
		assertEquals(serviceDTO.getPet().getId(), service.getPet().getId());
	}

	@Test
	public void petMappingTest() {
		Pet pet = new Pet();
		pet.setId(22l);
		pet.setBirthDate(new Date(4123153136l));
		pet.setBreed("breed");
		pet.setFur(FurType.MEDIUM);
		pet.setName("Name");

		List<Service> services = new ArrayList<Service>();
		Service s1 = new Service();
		s1.setId(3l);
		Service s2 = new Service();
		s2.setId(6l);
		services.add(s1);
		services.add(s2);
		pet.setServices(services);

		PetDTO petDTO = dozerMapper.map(pet, PetDTO.class);

		assertEquals(petDTO.getId(), pet.getId());
		assertEquals(petDTO.getBreed(), pet.getBreed());
		assertEquals(petDTO.getBirthDate(), pet.getBirthDate());
		assertEquals(petDTO.getFur().toString(), pet.getFur().toString());
		assertEquals(petDTO.getName(), pet.getName());

		assertEquals(petDTO.getServices().size(), pet.getServices().size());
		assertEquals(petDTO.getServices().get(0).getId(), pet.getServices()
				.get(0).getId());
		assertEquals(petDTO.getServices().get(1).getId(), pet.getServices()
				.get(1).getId());

	}

	@Test
	public void employeeMappingTest() {
		Employee employee = new Employee();
		Address address = new Address();
		address.setCity("Brno");
		address.setStreet("Jánská");
		address.setZip("123456");
		employee.setAddress(address);
		employee.setEmail("jan.novak@mail.com");
		employee.setId(1l);
		employee.setFirstName("Jan");
		employee.setLastName("Novák");
		employee.setPhone("123456789");
		employee.setSalary(3.14);

		EmployeeDTO employeeDTO = dozerMapper.map(employee, EmployeeDTO.class);

		assertEquals(employeeDTO.getId(), employee.getId());
		assertEquals(employeeDTO.getFirstName(), employee.getFirstName());
		assertEquals(employeeDTO.getAddress().getCity(), employee.getAddress()
				.getCity());
		assertEquals(employeeDTO.getAddress().getStreet(), employee
				.getAddress().getStreet());
		assertEquals(employeeDTO.getAddress().getZip(), employee.getAddress()
				.getZip());
		;
		assertEquals(employeeDTO.getEmail(), employee.getEmail());
		assertEquals(employeeDTO.getLastName(), employee.getLastName());
		assertEquals(employeeDTO.getPhone(), employee.getPhone());
		assertEquals(employeeDTO.getSalary(), employee.getSalary(), 0.0001);

	}
}
