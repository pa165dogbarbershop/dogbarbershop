package cz.fi.muni.pa165.dogbarbershop.service.test;

import cz.fi.muni.pa165.dogbarbershop.dao.CustomerDAO;
import cz.fi.muni.pa165.dogbarbershop.dto.CustomerDTO;
import cz.fi.muni.pa165.dogbarbershop.entity.Customer;
import cz.fi.muni.pa165.dogbarbershop.service.impl.CustomerServiceImpl;

import java.util.Arrays;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.Spy;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

/**
 *
 * @author David Balcak
 */
@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

    private CustomerServiceImpl customerService;
    private CustomerDAO mockedCustomerDAO;
    private Customer customer1;
    private Customer customer2;
    private CustomerDTO customer1DTO;
    private CustomerDTO customer2DTO;
    
    @Spy
    private Mapper dozerMapper = new DozerBeanMapper();
    
    @Before
    public void setup() {
        customerService = new CustomerServiceImpl();
        mockedCustomerDAO = mock(CustomerDAO.class);
        customerService.setCustomerDAO(mockedCustomerDAO);
        customerService.setMapper(dozerMapper);
        
        customer1 = new Customer();
        customer1.setFirstName("Jan");
        customer1.setLastName("Novak");
        customer1.setEmail("jan.novak@email.com");
        
        customer1DTO = dozerMapper.map(customer1, CustomerDTO.class);
        
        customer2 = new Customer();
        customer2.setFirstName("Petr");
        customer2.setLastName("Cech");
        customer2.setEmail("petrcech@example.com");
        
        customer2DTO = dozerMapper.map(customer2, CustomerDTO.class);
        
        // findCustomer
        when(mockedCustomerDAO.find(1L)).thenReturn(customer1);
        when(mockedCustomerDAO.find(2L)).thenReturn(customer2);
        
        // findAllCustomers
        when(mockedCustomerDAO.findAll()).thenReturn(Arrays.asList(customer1, customer2));
        
        // createCustomer
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Customer c = (Customer) args[0];
                customer1.setId(1L);
                return null;
            }
        }).when(mockedCustomerDAO).create(customer1);
        
        // updateCustomer
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Customer c = (Customer) args[0];
                customer2.setId(c.getId());
                customer2.setFirstName(c.getFirstName());
                customer2.setLastName(c.getLastName());
                customer2.setEmail(c.getEmail());
                return null;
            }
        }).when(mockedCustomerDAO).update(customer1);
        
        // deleteCustomer
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Customer c = (Customer) args[0];
                customer1.setId(null);
                return null;
            }
        }).when(mockedCustomerDAO).delete(customer1);
    }
    
    @Test
    public void createCustomer() {
        customerService.create(customer1DTO);
        assertNotNull(customer1.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void createNullCustomer() {
        customerService.create(null);
        fail("Created null Customer");
    }
    
    @Test
    public void updateCustomer() {
        customerService.update(customer1DTO);
        assertEquals(customer1, customer2);
        assertEquals(customer1.getFirstName(), customer2.getFirstName());
        assertEquals(customer1.getLastName(), customer2.getLastName());
        assertEquals(customer1.getEmail(), customer2.getEmail());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateNullCustomer() {
        customerService.update(null);
        fail("Updated null Customer");
    }
    
    @Test
    public void deleteCustomer() {
        customerService.delete(customer1DTO);
        assertNull(customer1.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void deleteNullCustomer() {
        customerService.delete(null);
        fail("Deleted null Customer");
    }
    
    @Test
    public void findCustomer() {
        assertNotNull(customerService.find(1L));
        assertEquals(customer1DTO, customerService.find(1L));
        assertNotNull(customerService.find(2L));
        assertEquals(customer2DTO, customerService.find(2L));
    }
    
    @Test
    public void findAllCustomers() {
        assertNotNull(customerService.findAll());
        assertEquals(Arrays.asList(customer1DTO, customer2DTO), customerService.findAll());
    }
}
