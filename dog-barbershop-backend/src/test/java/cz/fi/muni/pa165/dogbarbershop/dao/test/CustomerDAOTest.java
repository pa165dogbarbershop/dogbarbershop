package cz.fi.muni.pa165.dogbarbershop.dao.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.PersistenceException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import cz.fi.muni.pa165.dogbarbershop.dao.CustomerDAO;
import cz.fi.muni.pa165.dogbarbershop.entity.Address;
import cz.fi.muni.pa165.dogbarbershop.entity.Customer;

/**
 *
 * @author Petr Baláš
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class CustomerDAOTest {

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private JpaTransactionManager txManager;

    TransactionTemplate transactionTemplate;

    @Before
    public void setUp() throws Exception {
        transactionTemplate = new TransactionTemplate(txManager);

        transactionTemplate.execute(new TransactionCallback<Object>() {

            @Override
            public Object doInTransaction(TransactionStatus ts) {
                List<Customer> all = customerDAO.findAll();

                for(Customer c : all)
                    customerDAO.delete(c);

                return null;
            }
        });
    }

    @Test
    public void createCustomer() {
        final Customer c = newCustomer();
        Assert.assertNull(c.getId());

        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.create(c);

                return c;
            }
        });

        Assert.assertNotNull(c.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNullCustomer() {
        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.create(null);

                return null;
            }
        });

        Assert.fail("Created null Customer");
    }

    @Test(expected = PersistenceException.class)
    public void createCustomerWithId() {
        final Customer c = newCustomer();
        c.setId(1L);

        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.create(c);

                return c;
            }
        });

        Assert.fail("Created Customer with set Id");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createExistingCustomer() {
        final Customer c = newCustomer();

        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.create(c);
                customerDAO.create(c);

                return c;
            }
        });

        Assert.fail(c.toString() + " already in DB");
    }

    @Test
    public void createCustomerWithoutAttributes() {
        final Customer c = newCustomer();

        c.setFirstName(null);
        try {

            transactionTemplate.execute(new TransactionCallback<Customer>() {

                @Override
                public Customer doInTransaction(TransactionStatus ts) {
                    customerDAO.create(c);

                    return c;
                }
            });

            Assert.fail(c.toString() + " passed without firstName");
        } catch (PersistenceException ex) {}

        final Customer c2 = newCustomer();
        c2.setLastName(null);
        try {

            transactionTemplate.execute(new TransactionCallback<Customer>() {

                @Override
                public Customer doInTransaction(TransactionStatus ts) {
                    customerDAO.create(c2);

                    return c2;
                }
            });

            Assert.fail(c2.toString() + " passed without lastName");
        } catch (PersistenceException ex) {}

        final Customer c3 = newCustomer();
        c3.setEmail(null);
        try {

            transactionTemplate.execute(new TransactionCallback<Customer>() {

                @Override
                public Customer doInTransaction(TransactionStatus ts) {
                    customerDAO.create(c3);

                    return c3;
                }
            });

            Assert.fail(c3.toString() + " passed without email");
        } catch (PersistenceException ex) {}
    }

    @Test
    public void findCustomer() {
        final Customer c = newCustomer();
        Assert.assertNull(customerDAO.find(Long.MAX_VALUE));

        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.create(c);

                return c;
            }
        });

        Assert.assertEquals(customerDAO.find(c.getId()), c);
    }

    @Test
    public void findCustomerWithIncorrectArgument() {
        try {
            customerDAO.find(null);
            Assert.fail("Found Customer with null id");
        } catch (IllegalArgumentException e) {}
        try {
            customerDAO.find(Long.MIN_VALUE);
            Assert.fail("Found Customer with negative id");
        } catch (IllegalArgumentException e) {}
    }

    @Test
    public void findNonExistentCustomer() {
        Assert.assertNull(customerDAO.find(Long.MAX_VALUE));
    }

    @Test
    public void updateCustomer() {
        final Customer c = newCustomer();

        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.create(c);

                return c;
            }
        });

        Address a = new Address();
        a.setCity("Brno");
        c.setAddress(a);
        c.setEmail("j.novak@example.com");
        c.setCustomerNum(123456);

        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.update(c);

                return c;
            }
        });

        Customer updated = customerDAO.find(c.getId());
        Assert.assertEquals(updated, c);
        Assert.assertEquals(updated.getAddress(), a);
        Assert.assertEquals(updated.getEmail(), "j.novak@example.com");
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNullCustomer() {
        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.update(null);

                return null;
            }
        });
        Assert.fail("Updated null Customer");
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNonExistentCustomer() {
        transactionTemplate.execute(new TransactionCallback<Customer>() {
            Customer c = newCustomer();
            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.update(c);

                return c;
            }
        });
        Assert.fail("Updated non-existent Customer");
    }

    @Test
    public void deleteCustomer() {
        final Customer c = newCustomer();
        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.create(c);

                return c;
            }
        });

        long id = c.getId();
        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.delete(c);

                return c;
            }
        });
        Assert.assertNull(customerDAO.find(id));
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteNullCustomer() {
        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.delete(null);

                return null;
            }
        });
        Assert.fail("Deleted null Customer");
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteNonExistentCustomer() {
        transactionTemplate.execute(new TransactionCallback<Customer>() {
            Customer c = newCustomer();
            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.delete(c);

                return c;
            }
        });
        Assert.fail("Deleted non-existent Customer");
    }

    @Test
    public void findAllCustomers() {
        Assert.assertTrue(customerDAO.findAll().isEmpty());

        final Customer c1 = newCustomer();
        final Customer c2 = newCustomer();
        final Customer c3 = newCustomer();

        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.create(c1);

                return c1;
            }
        });
        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.create(c2);

                return c2;
            }
        });
        transactionTemplate.execute(new TransactionCallback<Customer>() {

            @Override
            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.create(c3);

                return c3;
            }
        });

        List<Customer> actual = customerDAO.findAll();
        List<Customer> expected = new ArrayList<Customer>(Arrays.asList(c1, c2, c3));
        Assert.assertEquals(actual.size(), expected.size());

        for (Customer c: new ArrayList<Customer>(expected)) {
            if (actual.contains(c)) {
                actual.remove(c);
                expected.remove(c);
            }
        }
        Boolean areEqual = actual.isEmpty() && expected.isEmpty();
        if (!areEqual) {
            Assert.fail("Got different list of customers than expected");
        }
    }

    private Customer newCustomer() {
        Customer c = new Customer();
        c.setFirstName("Jan");
        c.setLastName("Novak");
        c.setEmail("jan.novak@email.com");

        return c;
    }
}
