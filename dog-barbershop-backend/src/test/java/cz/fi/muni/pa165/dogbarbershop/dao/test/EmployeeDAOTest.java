package cz.fi.muni.pa165.dogbarbershop.dao.test;

import cz.fi.muni.pa165.dogbarbershop.dao.EmployeeDAO;
import cz.fi.muni.pa165.dogbarbershop.entity.Address;
import cz.fi.muni.pa165.dogbarbershop.entity.Customer;
import cz.fi.muni.pa165.dogbarbershop.entity.Employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.PersistenceException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * 
 * @author Petr Baláš
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class EmployeeDAOTest {
    
    @Autowired
    private EmployeeDAO employeeDAO;
    
    @Autowired
    private JpaTransactionManager txManager;
    
    TransactionTemplate transactionTemplate;

    @Before
    public void setUp() throws Exception {
        transactionTemplate = new TransactionTemplate(txManager);
        
        transactionTemplate.execute(new TransactionCallback<Object>() {

            public Object doInTransaction(TransactionStatus ts) {
                List<Employee> all = employeeDAO.findAll();
                
                for(Employee e : all)
                    employeeDAO.delete(e);
                
                return null;
            }
        });
    }
    
    @Test
    public void createEmployee() {
        final Employee e = newEmployee();
        Assert.assertNull(e.getId());
        
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.create(e);
                
                return e;
            }
        });
        
        Assert.assertNotNull(e.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void createNullEmployee() {
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.create(null);
                
                return null;
            }
        });
        Assert.fail("Created null Employee");
    }
    
    @Test(expected = PersistenceException.class)
    public void createEmployeeWithId() {
        final Employee e = newEmployee();
        e.setId(1L);
        
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.create(e);
                
                return e;
            }
        });
        
        Assert.fail("Created Employee with set Id");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void createExistingEmployee() {
        final Employee e = newEmployee();
        
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.create(e);
                employeeDAO.create(e);

                return e;
            }
        });

        Assert.fail(e.toString() + " already in DB");
    }
    
    @Test
    public void createEmployeeWithoutAttributes() {
        final Employee e = newEmployee();
        
        e.setFirstName(null);
        try {
            
            transactionTemplate.execute(new TransactionCallback<Employee>() {

                public Employee doInTransaction(TransactionStatus ts) {
                    employeeDAO.create(e);

                    return e;
                }
            });
            
            Assert.fail(e.toString() + " passed without firstName");
        } catch (PersistenceException ex) {}
        
        final Employee e2 = newEmployee();
        e2.setLastName(null);
        try {
            
            transactionTemplate.execute(new TransactionCallback<Employee>() {

                public Employee doInTransaction(TransactionStatus ts) {
                    employeeDAO.create(e2);

                    return e2;
                }
            });
            
            Assert.fail(e2.toString() + " passed without lastName");
        } catch (PersistenceException ex) {}
        
        final Employee e3 = newEmployee();
        e3.setEmail(null);
        try {
            
            transactionTemplate.execute(new TransactionCallback<Employee>() {

                public Employee doInTransaction(TransactionStatus ts) {
                    employeeDAO.create(e3);

                    return e3;
                }
            });
            
            Assert.fail(e3.toString() + " passed without email");
        } catch (PersistenceException ex) {}
    }
    
    @Test
    public void findEmployee() {
        final Employee e = newEmployee();
        Assert.assertNull(employeeDAO.find(Long.MAX_VALUE));
        
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.create(e);
                
                return e;
            }
        });
        
        Assert.assertEquals(employeeDAO.find(e.getId()), e);
    }
    
    @Test
    public void findEmployeeWithIncorrectArgument() {
        try {
            employeeDAO.find(null);
            Assert.fail("Found Employee with null id");
        } catch (IllegalArgumentException e) {}
        try {
            employeeDAO.find(Long.MIN_VALUE);
            Assert.fail("Found Employee with negative id");
        } catch (IllegalArgumentException e) {}
    }
    
    @Test
    public void findNonExistentEmployee() {
        Assert.assertNull(employeeDAO.find(Long.MAX_VALUE));
    }
    
    @Test
    public void updateEmployee() {
        final Employee e = newEmployee();
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.create(e);
                
                return e;
            }
        });
        
        Address a = new Address();
        a.setCity("Brno");
        e.setAddress(a);
        e.setEmail("j.novak@example.com");
        e.setSalary(20000.0);
        
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.update(e);
                
                return e;
            }
        });
        
        Employee updated = employeeDAO.find(e.getId());
        Assert.assertEquals(updated, e);
        Assert.assertEquals(updated.getAddress(), a);
        Assert.assertEquals(updated.getEmail(), "j.novak@example.com");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateNullEmployee() {
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.update(null);
                
                return null;
            }
        });
        Assert.fail("Updated null Employee");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateNonExistentEmployee() {
        transactionTemplate.execute(new TransactionCallback<Employee>() {
            Employee e = newEmployee();
            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.update(e);
                
                return e;
            }
        });
        Assert.fail("Updated non-existent Employee");
    }
    
    @Test
    public void deleteEmployee() {
        final Employee e = newEmployee();
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.create(e);
                
                return e;
            }
        });
        
        long id = e.getId();
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.delete(e);
                
                return e;
            }
        });
        Assert.assertNull(employeeDAO.find(id));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void deleteNullEmployee() {
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.delete(null);
                
                return null;
            }
        });
        Assert.fail("Deleted null Employee");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void deleteNonExistentEmployee() {
        transactionTemplate.execute(new TransactionCallback<Employee>() {
            Employee e = newEmployee();
            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.delete(e);
                
                return e;
            }
        });
        Assert.fail("Deleted non-existent Employee");
    }
    
    @Test
    public void findAllEmployees() {
        Assert.assertTrue(employeeDAO.findAll().isEmpty());
        
        final Employee e1 = newEmployee();
        final Employee e2 = newEmployee();
        final Employee e3 = newEmployee();
        
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.create(e1);
                
                return e1;
            }
        });
        
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.create(e2);
                
                return e2;
            }
        });
        
        transactionTemplate.execute(new TransactionCallback<Employee>() {

            public Employee doInTransaction(TransactionStatus ts) {
                employeeDAO.create(e3);
                
                return e3;
            }
        });
        
        List<Employee> actual = employeeDAO.findAll();
        List<Employee> expected = new ArrayList<Employee>(Arrays.asList(e1, e2, e3));
        Assert.assertEquals(actual.size(), expected.size());
        
        for (Employee e: new ArrayList<Employee>(expected)) {
            if (actual.contains(e)) {
                actual.remove(e);
                expected.remove(e);
            }
        }
        Boolean areEqual = actual.isEmpty() && expected.isEmpty();
        if (!areEqual) {
            Assert.fail("Got different list of employees than expected");
        }
    }
    
    private Employee newEmployee() {
        Employee e = new Employee();
        e.setFirstName("Jan");
        e.setLastName("Novak");
        e.setEmail("jan.novak@email.com");
        
        return e;
    }
}
