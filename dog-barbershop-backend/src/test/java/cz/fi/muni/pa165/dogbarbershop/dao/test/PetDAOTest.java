package cz.fi.muni.pa165.dogbarbershop.dao.test;

import cz.fi.muni.pa165.dogbarbershop.dao.CustomerDAO;
import cz.fi.muni.pa165.dogbarbershop.dao.PetDAO;
import cz.fi.muni.pa165.dogbarbershop.entity.Customer;
import cz.fi.muni.pa165.dogbarbershop.entity.Pet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author David Balcak
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class PetDAOTest {

    @Autowired
    private PetDAO petDAO;
    
    @Autowired
    private CustomerDAO customerDAO;
    
    @Autowired
    private JpaTransactionManager txManager;
    
    TransactionTemplate transactionTemplate;

    @Before
    public void setUp() throws Exception {
        transactionTemplate = new TransactionTemplate(txManager);
        
        transactionTemplate.execute(new TransactionCallback<Object>() {

            public Object doInTransaction(TransactionStatus ts) {
                List<Customer> allC = customerDAO.findAll();
                List<Pet> allP = petDAO.findAll();
                
                for(Customer c : allC)
                    customerDAO.delete(c);
                
                for(Pet p : allP)
                    petDAO.delete(p);
                
                return null;
            }
        });
    }
    
    @Test
    public void createPet() {
        final Pet p = newPet();
        Assert.assertNull(p.getId());
        
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.create(p);
                
                return p;
            }
        });
        
        Assert.assertNotNull(p.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void createNullPet() {
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.create(null);
                
                return null;
            }
        });
        
        Assert.fail("Created null Pet");
    }
    
    @Test(expected = PersistenceException.class)
    public void createPetWithId() {
        final Pet p = newPet();
        p.setId(1L);
        
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.create(p);
                
                return p;
            }
        });
        
        Assert.fail("Created Pet with set Id");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void createExistingPet() {
        final Pet p = newPet();
        
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.create(p);
                petDAO.create(p);
                
                return p;
            }
        });

        Assert.fail(p.toString() + " already in DB");
    }
    
    @Test
    public void createPetWithoutAttributes() {
        final Pet p = newPet();
        
        p.setName(null);
        try {
            
            transactionTemplate.execute(new TransactionCallback<Pet>() {

                public Pet doInTransaction(TransactionStatus ts) {
                    petDAO.create(p);

                    return p;
                }
            });
            
            Assert.fail(p.toString() + " passed without name");
        } catch (PersistenceException ex) {}
        
        final Pet p2 = newPet();
        p2.setBreed(null);
        try {
            
            transactionTemplate.execute(new TransactionCallback<Pet>() {

                public Pet doInTransaction(TransactionStatus ts) {
                    petDAO.create(p2);

                    return p2;
                }
            });
            
            Assert.fail(p2.toString() + " passed without breed");
        } catch (PersistenceException ex) {}
        
        final Pet p3 = newPet();
        p3.setBirthDate(null);
        try {
            
            transactionTemplate.execute(new TransactionCallback<Pet>() {

                public Pet doInTransaction(TransactionStatus ts) {
                    petDAO.create(p3);

                    return p3;
                }
            });
            
            Assert.fail(p3.toString() + " passed without birthDate");
        } catch (PersistenceException ex) {}
    }
    
    @Test
    public void findPet() {
        final Pet p = newPet();
        Assert.assertNull(petDAO.find(Long.MAX_VALUE));
        
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.create(p);
                
                return p;
            }
        });
        
        Assert.assertEquals(petDAO.find(p.getId()), p);
    }
    
    @Test
    public void findPetWithIncorrectArgument() {
        try {
            petDAO.find(null);
            Assert.fail("Found Pet with null id");
        } catch (IllegalArgumentException e) {}
        try {
            petDAO.find(Long.MIN_VALUE);
            Assert.fail("Found Pet with negative id");
        } catch (IllegalArgumentException e) {}
    }
    
    @Test
    public void findNonExistentPet() {
        Assert.assertNull(petDAO.find(Long.MAX_VALUE));
    }
    
    @Test
    public void updatePet() {
        final Pet p = newPet();
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.create(p);
                
                return p;
            }
        });
        
        final Customer c = new Customer();
        c.setFirstName("Jan");
        c.setLastName("Novak");
        c.setEmail("jan.novak@email.com");
        
        transactionTemplate.execute(new TransactionCallback<Customer>() {

            public Customer doInTransaction(TransactionStatus ts) {
                customerDAO.create(c);
                
                return c;
            }
        });
        
        p.setFur(Pet.FurType.SHORT);
        p.setCustomer(c);
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.update(p);
                
                return p;
            }
        });
        
        Pet updated = petDAO.find(p.getId());
        Assert.assertEquals(updated, p);
        Assert.assertEquals(updated.getFur(), Pet.FurType.SHORT);
        Assert.assertEquals(updated.getCustomer(), c);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateNullPet() {
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.update(null);
                
                return null;
            }
        });
        Assert.fail("Updated null Pet");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateNonExistentPet() {
        transactionTemplate.execute(new TransactionCallback<Pet>() {
            Pet p = newPet();
            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.update(p);
                
                return p;
            }
        });
        Assert.fail("Updated non-existent Pet");
    }
    
    @Test
    public void deletePet() {
        final Pet p = newPet();
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.create(p);
                
                return p;
            }
        });
        
        long id = p.getId();
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.delete(p);
                
                return p;
            }
        });
        Assert.assertNull(petDAO.find(id));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void deleteNullPet() {
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.delete(null);
                
                return null;
            }
        });
        Assert.fail("Deleted null Pet");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void deleteNonExistentPet() {
        transactionTemplate.execute(new TransactionCallback<Pet>() {
            Pet p = newPet();
            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.delete(p);
                
                return p;
            }
        });
        Assert.fail("Deleted non-existent Pet");
    }
    
    @Test
    public void findAllPets() {
        Assert.assertTrue(petDAO.findAll().isEmpty());
        
        final Pet p1 = newPet();
        final Pet p2 = newPet();
        final Pet p3 = newPet();
        
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.create(p1);
                
                return p1;
            }
        });
        
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.create(p2);
                
                return p2;
            }
        });
        
        transactionTemplate.execute(new TransactionCallback<Pet>() {

            public Pet doInTransaction(TransactionStatus ts) {
                petDAO.create(p3);
                
                return p3;
            }
        });
        
        List<Pet> actual = petDAO.findAll();
        List<Pet> expected = new ArrayList<Pet>(Arrays.asList(p1, p2, p3));
        Assert.assertEquals(actual.size(), expected.size());
        
        for (Pet p: new ArrayList<Pet>(expected)) {
            if (actual.contains(p)) {
                actual.remove(p);
                expected.remove(p);
            }
        }
        Boolean areEqual = actual.isEmpty() && expected.isEmpty();
        if (!areEqual) {
            Assert.fail("Got different list of pets than expected");
        }
    }
    
    private Pet newPet() {
        Pet p = new Pet();
        p.setName("Happy");
        p.setBreed("Yorkshire Terrier");
        p.setBirthDate(new Date());
        p.setFur(Pet.FurType.MEDIUM);
        
        return p;
    }
}
