package cz.fi.muni.pa165.dogbarbershop.service.test;

import cz.fi.muni.pa165.dogbarbershop.dao.EmployeeDAO;
import cz.fi.muni.pa165.dogbarbershop.dto.EmployeeDTO;
import cz.fi.muni.pa165.dogbarbershop.entity.Employee;
import cz.fi.muni.pa165.dogbarbershop.service.impl.EmployeeServiceImpl;

import java.util.Arrays;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.Spy;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

/**
 *
 * @author David Balcak
 */
@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {
    
    private EmployeeServiceImpl employeeService;
    private EmployeeDAO mockedEmployeeDAO;
    private Employee employee1;
    private Employee employee2;
    private EmployeeDTO employee1DTO;
    private EmployeeDTO employee2DTO;
    
    @Spy
    private Mapper dozerMapper = new DozerBeanMapper();
    
    @Before
    public void setup() {
        employeeService = new EmployeeServiceImpl();
        mockedEmployeeDAO = mock(EmployeeDAO.class);
        employeeService.setEmployeeDAO(mockedEmployeeDAO);
        employeeService.setMapper(dozerMapper);
        
        employee1 = new Employee();
        employee1.setFirstName("Jan");
        employee1.setLastName("Novak");
        employee1.setEmail("jan.novak@email.com");
        
        employee1DTO = dozerMapper.map(employee1, EmployeeDTO.class);
        
        employee2 = new Employee();
        employee2.setFirstName("Petr");
        employee2.setLastName("Cech");
        employee2.setEmail("petrcech@example.com");
        
        employee2DTO = dozerMapper.map(employee2, EmployeeDTO.class);
        
        // findEmployee
        when(mockedEmployeeDAO.find(1L)).thenReturn(employee1);
        when(mockedEmployeeDAO.find(2L)).thenReturn(employee2);
        
        // findAllEmployees
        when(mockedEmployeeDAO.findAll()).thenReturn(Arrays.asList(employee1, employee2));
        
        // createEmployee
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Employee e = (Employee) args[0];
                employee1.setId(1L);
                return null;
            }
        }).when(mockedEmployeeDAO).create(employee1);
        
        // updateEmployee
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Employee e = (Employee) args[0];
                employee2.setId(e.getId());
                employee2.setFirstName(e.getFirstName());
                employee2.setLastName(e.getLastName());
                employee2.setEmail(e.getEmail());
                return null;
            }
        }).when(mockedEmployeeDAO).update(employee1);
        
        // deleteEmployee
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Employee e = (Employee) args[0];
                employee1.setId(null);
                return null;
            }
        }).when(mockedEmployeeDAO).delete(employee1);
    }
    
    @Test
    public void createEmployee() {
        employeeService.create(employee1DTO);
        assertNotNull(employee1.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void createNullEmployee() {
        employeeService.create(null);
        fail("Created null Employee");
    }
    
    @Test
    public void updateEmployee() {
        employeeService.update(employee1DTO);
        assertEquals(employee1, employee2);
        assertEquals(employee1.getFirstName(), employee2.getFirstName());
        assertEquals(employee1.getLastName(), employee2.getLastName());
        assertEquals(employee1.getEmail(), employee2.getEmail());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateNullEmployee() {
        employeeService.update(null);
        fail("Updated null Employee");
    }
    
    @Test
    public void deleteEmployee() {
        employeeService.delete(employee1DTO);
        assertNull(employee1.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void deleteNullEmployee() {
        employeeService.delete(null);
        fail("Deleted null Employee");
    }
    
    @Test
    public void findEmployee() {
        assertNotNull(employeeService.find(1L));
        assertEquals(employee1DTO, employeeService.find(1L));
        assertNotNull(employeeService.find(2L));
        assertEquals(employee2DTO, employeeService.find(2L));
    }
    
    @Test
    public void findAllEmployees() {
        assertNotNull(employeeService.findAll());
        assertEquals(Arrays.asList(employee1DTO, employee2DTO), employeeService.findAll());
    }
}
