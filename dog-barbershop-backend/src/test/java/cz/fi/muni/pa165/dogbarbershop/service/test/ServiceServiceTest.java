package cz.fi.muni.pa165.dogbarbershop.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import cz.fi.muni.pa165.dogbarbershop.dao.ServiceDAO;
import cz.fi.muni.pa165.dogbarbershop.dto.ServiceDTO;
import cz.fi.muni.pa165.dogbarbershop.entity.Service;
import cz.fi.muni.pa165.dogbarbershop.service.impl.ServiceServiceImpl;

/**
 *
 * @author David Balcak
 */
@RunWith(MockitoJUnitRunner.class)
public class ServiceServiceTest {
    
    private ServiceServiceImpl serviceService;
    private ServiceDAO mockedServiceDAO;
    private Service service1;
    private Service service2;
    private ServiceDTO service1DTO;
    private ServiceDTO service2DTO;
    
    @Spy
    private Mapper dozerMapper = new DozerBeanMapper();
    
    @Before
    public void setup() {
        serviceService = new ServiceServiceImpl();
        mockedServiceDAO = mock(ServiceDAO.class);
        serviceService.setServiceDAO(mockedServiceDAO);
        serviceService.setMapper(dozerMapper);
        
        service1 = new Service();
        service1.setName("Fur Trim");
        service1.setDuration(600);
        service1.setPrice(200.0);
        
        service1DTO = dozerMapper.map(service1, ServiceDTO.class);
        
        service2 = new Service();
        service2.setName("Fur Coloring");
        service2.setDuration(900);
        service2.setPrice(500.0);
        
        service2DTO = dozerMapper.map(service2, ServiceDTO.class);
        
        // findService
        when(mockedServiceDAO.find(1L)).thenReturn(service1);
        when(mockedServiceDAO.find(2L)).thenReturn(service2);
        
        // findAllServices
        when(mockedServiceDAO.findAll()).thenReturn(Arrays.asList(service1, service2));
        
        // createService
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Service s = (Service) args[0];
                service1.setId(1L);
                return null;
            }
        }).when(mockedServiceDAO).create(service1);
        
        // updateService
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Service s = (Service) args[0];
                service2.setId(s.getId());
                service2.setName(s.getName());
                service2.setDuration(s.getDuration());
                service2.setPrice(s.getPrice());
                return null;
            }
        }).when(mockedServiceDAO).update(service1);
        
        // deleteService
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Service s = (Service) args[0];
                service1.setId(null);
                return null;
            }
        }).when(mockedServiceDAO).delete(service1);
    }
    
    @Test
    public void createService() {
        serviceService.create(service1DTO);
        assertNotNull(service1.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void createNullService() {
        serviceService.create(null);
        fail("Created null Service");
    }
    
    @Test
    public void updateService() {
        serviceService.update(service1DTO);
        assertEquals(service1, service2);
        assertEquals(service1.getName(), service2.getName());
        assertEquals(service1.getDuration(), service2.getDuration());
        assertEquals(service1.getPrice(), service2.getPrice(), 0.0);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateNullService() {
        serviceService.update(null);
        fail("Updated null Service");
    }
    
    @Test
    public void deleteService() {
        serviceService.delete(service1DTO);
        assertNull(service1.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void deleteNullService() {
        serviceService.delete(null);
        fail("Deleted null Service");
    }
    
    @Test
    public void findService() {
        assertNotNull(serviceService.find(1L));
        assertEquals(service1DTO, serviceService.find(1L));
        assertNotNull(serviceService.find(2L));
        assertEquals(service2DTO, serviceService.find(2L));
    }
    
    @Test
    public void findAllServices() {
        assertNotNull(serviceService.findAll());
        assertEquals(Arrays.asList(service1DTO, service2DTO), serviceService.findAll());
    }
}
