package cz.fi.muni.pa165.dogbarbershop.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.fi.muni.pa165.dogbarbershop.dao.ServiceDAO;
import cz.fi.muni.pa165.dogbarbershop.entity.Service;

/**
 * Implements {@link ServiceDAO} interface for accessing Service
 * objects. The implementation uses JPA for accessing pet entities
 * in the database.
 *
 * @author Petr Baláš
 */
public class JpaServiceDAOImpl implements ServiceDAO {
	
	static final Logger logger = LoggerFactory.getLogger(JpaServiceDAOImpl.class);
    
    private EntityManager entityManager;

    @Override
    public void create(Service service) {
        if (service == null) {
            IllegalArgumentException e = new IllegalArgumentException("Service cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (service.getId() != null && service.equals(getEntityManager().find(Service.class, service.getId()))) {
            IllegalArgumentException e = new IllegalArgumentException(service.toString() + " already in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        logger.debug("Storing service into DB: {}", service);
        getEntityManager().persist(service);
    }
    
    @Override
    public Service find(Long id) {
        if (id == null) {
            IllegalArgumentException e = new IllegalArgumentException("Id cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
            		
        }
        if (id < 0) {
            IllegalArgumentException e =  new IllegalArgumentException("Id cannot be negative");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        Service service = getEntityManager().find(Service.class, id);
        logger.debug("Service with id={} was fetched from DB: ", service);
        
        return service;
    }

    @Override
    public void update(Service service) {
        if (service == null) {
            IllegalArgumentException e = new IllegalArgumentException("Service cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (service.getId() == null || getEntityManager().find(Service.class, service.getId()) == null) {
        	IllegalArgumentException e = new IllegalArgumentException("Service not found in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        logger.debug("Service updated: {}", service);
        getEntityManager().merge(service);
    }

    @Override
    public void delete(Service service) {
        if (service == null) {
        	IllegalArgumentException e = new IllegalArgumentException("Service cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (service.getId() == null || getEntityManager().find(Service.class, service.getId()) == null) {
        	IllegalArgumentException e = new IllegalArgumentException("Service not found in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        Service managedService = getEntityManager().find(Service.class, service.getId());

        logger.debug("Removing service from DB: {}", service);
        getEntityManager().remove(managedService);
    }

    @Override
    public List<Service> findAll() {
        List<Service> list = getEntityManager().createQuery("from Service").getResultList();
        
        logger.debug("Getting all services.");
        
        return list;
    }

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param entityManager the entityManager to set
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
