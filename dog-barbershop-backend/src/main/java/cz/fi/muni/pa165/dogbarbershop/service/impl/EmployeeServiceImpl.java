/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.pa165.dogbarbershop.service.impl;

import cz.fi.muni.pa165.dogbarbershop.dao.EmployeeDAO;
import cz.fi.muni.pa165.dogbarbershop.dto.EmployeeDTO;
import cz.fi.muni.pa165.dogbarbershop.entity.Employee;
import cz.fi.muni.pa165.dogbarbershop.service.EmployeeService;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.dozer.MappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Petr Baláš
 */
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDAO employeeDAO;
    private Mapper mapper;
    
    static private final Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Override
    @Transactional
    public void create(EmployeeDTO employeeDTO) {
            if (employeeDTO == null){
                    throw new IllegalArgumentException("Employee is null");
            }

            Employee employee = null;
            try {
                employee = mapper.map(employeeDTO, Employee.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+employeeDTO);
            }
            employeeDAO.create(employee);
    }

    @Override
    @Transactional
    public void update(EmployeeDTO employeeDTO) {
            if (employeeDTO == null){
                    throw new IllegalArgumentException("Employee is null");
            }

            Employee employee = null;
            try {
                employee = mapper.map(employeeDTO, Employee.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+employeeDTO);
            }
            employeeDAO.update(employee);
    }

    @Override
    @Transactional
    public void delete(EmployeeDTO employeeDTO) {
            if (employeeDTO == null){
                    throw new IllegalArgumentException("Employee is null");
            }

            Employee employee = null;
            try {
                employee = mapper.map(employeeDTO, Employee.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+employeeDTO);
            }
            employeeDAO.delete(employee);
    }

    @Override
    @Transactional
    public EmployeeDTO find(Long id) {
            if (id == null){
                    throw new IllegalArgumentException("ID is null");
            }

            Employee employee = employeeDAO.find(id);
            EmployeeDTO employeeDTO = null;
            try {
                employeeDTO = mapper.map(employee, EmployeeDTO.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity with 'id="+id+"'");
            }

            return employeeDTO;
    }

    @Override
    @Transactional
    public List<EmployeeDTO> findAll() {
            List<Employee> employees = employeeDAO.findAll();
            List<EmployeeDTO> employeesDTO = new ArrayList<EmployeeDTO>();

            for (Employee e : employees){
                    EmployeeDTO employeeDTO = null;
            
                    try {
                        employeeDTO = mapper.map(e, EmployeeDTO.class);
                    } catch(MappingException ex) {
                        logger.error("Could not map entity "+e);
                    }
                    
                    if(employeeDTO != null)
                        employeesDTO.add(employeeDTO);
            }

            return employeesDTO;
    }

    public EmployeeDAO getEmployeeDAO() {
            return employeeDAO;
    }

    public void setEmployeeDAO(EmployeeDAO employeeDAO) {
            this.employeeDAO = employeeDAO;
    }

    public Mapper getMapper() {
            return mapper;
    }

    public void setMapper(Mapper mapper) {
            this.mapper = mapper;
    }
}
