/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.pa165.dogbarbershop.service.impl;

import cz.fi.muni.pa165.dogbarbershop.dao.ServiceDAO;
import cz.fi.muni.pa165.dogbarbershop.dto.ServiceDTO;
import cz.fi.muni.pa165.dogbarbershop.entity.Pet;
import cz.fi.muni.pa165.dogbarbershop.entity.Service;
import cz.fi.muni.pa165.dogbarbershop.service.ServiceService;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.dozer.MappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Petr Baláš
 */
@org.springframework.stereotype.Service
@Transactional
public class ServiceServiceImpl implements ServiceService {
    private ServiceDAO serviceDAO;
    private Mapper mapper;
    
    static private final Logger logger = LoggerFactory.getLogger(ServiceServiceImpl.class);

    @Override
    @Transactional
    public void create(ServiceDTO serviceDTO) {
            if (serviceDTO == null){
                    throw new IllegalArgumentException("Service is null");
            }

            Service service = null;
            try {
                service = mapper.map(serviceDTO, Service.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+serviceDTO);
            }
            serviceDAO.create(service);
    }

    @Override
    @Transactional
    public void update(ServiceDTO serviceDTO) {
            if (serviceDTO == null){
                    throw new IllegalArgumentException("Service is null");
            }

            Service service = null;
            try {
                service = mapper.map(serviceDTO, Service.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+serviceDTO);
            }
            serviceDAO.update(service);
    }

    @Override
    @Transactional
    public void delete(ServiceDTO serviceDTO) {
            if (serviceDTO == null){
                    throw new IllegalArgumentException("Service is null");
            }
            
            Service service = null;
            try {
                service = mapper.map(serviceDTO, Service.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+serviceDTO);
            }
            serviceDAO.delete(service);
    }

    @Override
    @Transactional
    public ServiceDTO find(Long id) {
            if (id == null){
                    throw new IllegalArgumentException("ID is null");
            }

            Service pet = serviceDAO.find(id);
            ServiceDTO serviceDTO = null;
            try {
                serviceDTO = mapper.map(pet, ServiceDTO.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity with 'id="+id+"'");
            }

            return serviceDTO;
    }

    @Override
    @Transactional
    public List<ServiceDTO> findAll() {
            List<Service> services = serviceDAO.findAll();
            List<ServiceDTO> servicesDTO = new ArrayList<ServiceDTO>();

            for (Service s : services){
                    ServiceDTO serviceDTO = null;
            
                    try {
                        serviceDTO = mapper.map(s, ServiceDTO.class);
                    } catch(MappingException ex) {
                        logger.error("Could not map entity "+s);
                    }
                    
                    if(serviceDTO != null)
                        servicesDTO.add(serviceDTO);
            }

            return servicesDTO;
    }

    public ServiceDAO getServiceDAO() {
            return serviceDAO;
    }

    public void setServiceDAO(ServiceDAO serviceDAO) {
            this.serviceDAO = serviceDAO;
    }

    public Mapper getMapper() {
            return mapper;
    }

    public void setMapper(Mapper mapper) {
            this.mapper = mapper;
    }
}
