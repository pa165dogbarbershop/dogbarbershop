package cz.fi.muni.pa165.dogbarbershop.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Entity representing Customer. Inherits all the attributes
 * of Person and adds customer number and the set of pets they own.
 *
 * @author Petr Baláš
 */
@Entity
@XmlRootElement
public class Customer extends Person implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Column
    private int customerNum;
    
    @OneToMany(fetch=FetchType.EAGER, mappedBy="customer", cascade = CascadeType.REMOVE)
    private Set<Pet> pets;
    
    public Customer() {
        super();
    }

    /**
     * Returns customer number
     * 
     * @return Unique customer number
     */
    public int getCustomerNum() {
        return customerNum;
    }

    /**
     * Sets customer number
     * 
     * @param customerNum Unique customer number
     */
    public void setCustomerNum(int customerNum) {
        this.customerNum = customerNum;
    }

    /**
     * Returns the list of customer's pets
     * 
     * @return List of customer's pets
     */
    @XmlTransient
    public Set<Pet> getPets() {
        return pets;
    }

    /**
     * Sets the list of customer's pets
     * 
     * @param pets List of customer's pets
     */
    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }
    
    @Override
    public String toString() {
        return super.toString() + ", customerNum=" + customerNum + "]";
    }
}
