package cz.fi.muni.pa165.dogbarbershop.dao;

import cz.fi.muni.pa165.dogbarbershop.entity.Employee;

import java.util.List;

/**
 * EmployeeDAO is the interface for accessing Employee objects
 *
 * @author David Balcak
 */
public interface EmployeeDAO {
    
    /**
     * Creates a new employee in the database
     * 
     * @param employee 
     * @throws IllegalArgumentException if employee is null
     * @throws IllegalArgumentException if employee is already in DB
     */
    public void create(Employee employee);
    
    /**
     * Finds and returns employee with given id in the database,
     * otherwise returns null
     * 
     * @param id of the employee to find
     * @return Found employee or null if not found
     * @throws IllegalArgumentException if id is negative or null
     */
    public Employee find(Long id);
    
    /**
     * Updates existing employee in the database
     * 
     * @param employee
     * @throws IllegalArgumentException if employee is null
     * @throws IllegalArgumentException if employee is not in DB
     */
    public void update(Employee employee);
    
    /**
     * Deletes employee from the database
     * 
     * @param employee 
     * @throws IllegalArgumentException if employee is null
     * @throws IllegalArgumentException if employee is not in DB
     */
    public void delete(Employee employee);
    
    
    /**
     * Finds and returns all the employees in the database, or returns
     * empty list if the database is empty
     * 
     * @return List of employees
     */
    public List<Employee> findAll();
}
