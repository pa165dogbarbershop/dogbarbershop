package cz.fi.muni.pa165.dogbarbershop.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Entity representing Pet.
 *
 * @author Jakub Knetl
 */
@Entity
@XmlRootElement
public class Pet implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String breed;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date birthDate; 

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private FurType fur;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="customer_id")
    private Customer customer = null;
    
    @OneToMany(fetch=FetchType.EAGER, mappedBy="pet", cascade = CascadeType.REMOVE)
    private List<Service> services;
    
    public Pet() {
    }
    
    /**
     * Pet fur type enumerator
     */
    public enum FurType {
        
        SHORT,

        MEDIUM,

        LONG;
    }
    
    /**
     * Returns pet id
     *
     * @return Id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets pet id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * Returns pet name
     *
     * @return Name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets pet name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Returns pet breed
     *
     * @return Breed
     */
    public String getBreed() {
        return breed;
    }

    /**
     * Sets pet breed
     *
     * @param breed
     */
    public void setBreed(String breed) {
        this.breed = breed;
    }
    
    /**
     * Returns pet birthday
     *
     * @return Birthday
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Sets pet birthday
     *
     * @param birthDate Birthday
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    
    /**
     * Returns pet fur type
     *
     * @return Fur type
     */
    public FurType getFur() {
        return fur;
    }

    /**
     * Sets pet fur type
     *
     * @param fur Fur type
     */
    public void setFur(FurType fur) {
        this.fur = fur;
    }
    
    /**
     * Returns pet customer (owner)
     * 
     * @return Owner of the pet
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets pet customer (owner)
     * 
     * @param customer Pet owner
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * Returns the list of pet services
     * 
     * @return List of pet services
     */
    @XmlTransient
    public List<Service> getServices() {
        return services;
    }

    /**
     * Sets the list of pet services
     * 
     * @param services List of pet services
     */
    public void setServices(List<Service> services) {
        this.services = services;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pet other = (Pet) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String pet = "Pet [id=" + id + ", name=" + name + ", breed=" + breed
                + ", birthDate=" + birthDate + ", fur=" + fur;
        if (customer != null) {
            pet += ", customer=" + customer.getFirstName() + " "
                    + customer.getLastName();
        }
        return pet + "]";
    }
}
