/**
 *
 */
package cz.fi.muni.pa165.dogbarbershop.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.fi.muni.pa165.dogbarbershop.dao.AccountDAO;
import cz.fi.muni.pa165.dogbarbershop.dao.impl.JpaAccountDAOImpl;
import cz.fi.muni.pa165.dogbarbershop.dto.AccountDTO;
import cz.fi.muni.pa165.dogbarbershop.entity.Account;
import cz.fi.muni.pa165.dogbarbershop.service.AccountService;
import java.util.Collection;
import org.dozer.MappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author Jakub Knetl
 *
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    private AccountDAO accountDAO;
    private Mapper mapper;
    
    static private final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Override
    @Transactional
    public void create(AccountDTO accountDTO) {
            if (accountDTO == null){
                    throw new IllegalArgumentException("Account is null");
            }

            Account account = null;
            try {
                account = mapper.map(accountDTO, Account.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+accountDTO);
            }
            accountDAO.create(account);
    }

    @Override
    @Transactional
    public void update(AccountDTO accountDTO) {
            if (accountDTO == null){
                    throw new IllegalArgumentException("Account is null");
            }

            Account account = null;
            try {
                account = mapper.map(accountDTO, Account.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+accountDTO);
            }
            accountDAO.update(account);
    }

    @Override
    @Transactional
    public void delete(AccountDTO accountDTO) {
            if (accountDTO == null){
                    throw new IllegalArgumentException("Account is null");
            }


            Account account = null;
            try {
                account = mapper.map(accountDTO, Account.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+accountDTO);
            }
            
            accountDAO.delete(account);
    }

    @Override
    @Transactional
    public AccountDTO find(Long id) {
            if (id == null){
                    throw new IllegalArgumentException("ID is null");
            }

            Account account = accountDAO.find(id);
            AccountDTO accountDTO = null;
            
            try {
                accountDTO = mapper.map(account, AccountDTO.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity with 'id="+id+"'");
            }

            return accountDTO;
    }
    
    @Override
    @Transactional
    public AccountDTO find(String login) {
            if (login == null){
                    throw new IllegalArgumentException("LOGIN is null");
            }

            Account account = accountDAO.find(login);
            AccountDTO accountDTO = null;
            
            try {
                accountDTO = mapper.map(account, AccountDTO.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity with 'login="+login+"'");
            }

            return accountDTO;
    }

    @Override
    @Transactional
    public List<AccountDTO> findAll() {
            List<Account> accounts = accountDAO.findAll();
            List<AccountDTO> accountsDTO = new ArrayList<AccountDTO>();

            for (Account a : accounts){
                    AccountDTO accountDTO = null;
            
                    try {
                        accountDTO = mapper.map(a, AccountDTO.class);
                    } catch(MappingException ex) {
                        logger.error("Could not map entity "+a);
                    }
                    
                    if(accountDTO != null)
                        accountsDTO.add(accountDTO);
            }

            return accountsDTO;
    }
    
    @Override
    public boolean hasRole(String role) {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
        SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean hasRole = false;
        for (GrantedAuthority authority : authorities) {
            hasRole = authority.getAuthority().equals(role);
            if (hasRole) {
                 break;
            }
        }
        return hasRole;
    }
    
	@Override
	public AccountDTO getLoggedAccount() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth.getName().equals("admin"))
			return null;
		else
			return find(auth.getName());
	}

    public AccountDAO getAccountDAO() {
            return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO) {
            this.accountDAO = accountDAO;
    }

    public Mapper getMapper() {
            return mapper;
    }

    public void setMapper(Mapper mapper) {
            this.mapper = mapper;
    }
}
