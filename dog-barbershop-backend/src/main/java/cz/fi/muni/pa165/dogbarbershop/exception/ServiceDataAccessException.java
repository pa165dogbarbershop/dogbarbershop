/**
 * 
 */
package cz.fi.muni.pa165.dogbarbershop.exception;

import org.springframework.dao.DataAccessException;

/**
 * Service layer exception.
 * 
 * @author Jakub Knetl
 *
 */
public class ServiceDataAccessException extends DataAccessException {

	public ServiceDataAccessException(String msg) {
		super(msg);
	}
	
	public ServiceDataAccessException(String msg, Throwable cause) {
		super(msg, cause);
	}
	

}
