/**
 *
 */
package cz.fi.muni.pa165.dogbarbershop.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.fi.muni.pa165.dogbarbershop.dao.AccountDAO;
import cz.fi.muni.pa165.dogbarbershop.entity.Account;

/**
 * @author jknetl
 *
 */
public class JpaAccountDAOImpl implements AccountDAO {

    static final Logger logger = LoggerFactory.getLogger(JpaAccountDAOImpl.class);

    private EntityManager entityManager;

    @Override
    public void create(Account account) {

        if (account == null){
            IllegalArgumentException e = new IllegalArgumentException("Account cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (account.getId() != null && account.equals(entityManager.find(Account.class, account.getId()))){
            IllegalArgumentException e = new IllegalArgumentException(account.toString() + " already in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        logger.debug("Persisting account in DB: {}", account);
    }


    @Override
    public Account find(Long id) {
        if (id == null) {
            IllegalArgumentException e = new IllegalArgumentException("Id cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        if (id < 0) {
            IllegalArgumentException e = new IllegalArgumentException("Id cannot be negative");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        Account account = getEntityManager().find(Account.class, id);
        logger.debug("Account with id={} fetched from DB: {}", id, account);

        return account;
    }
    
    @Override
    public Account find(String login) {
        if (login == null) {
            IllegalArgumentException e = new IllegalArgumentException("Login cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        List<Account> accounts = findAll();
        Account account = null;
        
        for(Account acc : accounts) {
            if(acc.getLogin().equals(login))
                account = acc;
        }
        
        logger.debug("Account with login={} fetched from DB: {}", login, account);

        return account;
    }


    @Override
    public void update(Account account) {
        if (account == null) {
            IllegalArgumentException e = new IllegalArgumentException("Account cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (account.getId() == null || getEntityManager().find(Account.class, account.getId()) == null) {
            IllegalArgumentException e = new IllegalArgumentException("Account not found in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        logger.debug("Updating account: ", account);
        getEntityManager().merge(account);

    }


    @Override
    public void delete(Account account) {
        if (account == null) {
            IllegalArgumentException e = new IllegalArgumentException("Account cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (account.getId() == null || getEntityManager().find(Account.class, account.getId()) == null) {
            IllegalArgumentException e = new IllegalArgumentException("Account not found in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        Account managedAccount = getEntityManager().find(Account.class, account.getId());

        logger.debug("Removing account from DB: {}", account);
        getEntityManager().remove(managedAccount);

    }


    @Override
    public List<Account> findAll() {
        List<Account> list = getEntityManager().createQuery("from Account").getResultList();
        logger.debug("Getting all accounts from DB");

        return list;
    }


    public EntityManager getEntityManager() {
        return entityManager;
    }


    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
