package cz.fi.muni.pa165.dogbarbershop.dao.impl;

import cz.fi.muni.pa165.dogbarbershop.dao.PetDAO;
import cz.fi.muni.pa165.dogbarbershop.entity.Pet;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implements {@link PetDAO} interface for accessing Pet
 * objects. The implementation uses JPA for accessing pet entities
 * in the database.
 *
 * @author Petr Baláš
 */
public class JpaPetDAOImpl implements PetDAO {
	
	static final Logger logger = LoggerFactory.getLogger(JpaPetDAOImpl.class);

    private EntityManager entityManager;

    @Override
    public void create(Pet pet) {
        if (pet == null) {
            IllegalArgumentException e = new IllegalArgumentException("Pet cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (pet.getId() != null && pet.equals(getEntityManager().find(Pet.class, pet.getId()))) {
            IllegalArgumentException e = new IllegalArgumentException(pet.toString() + " already in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        
        logger.debug("Persisting pet into DB: {}", pet);
        getEntityManager().persist(pet);
    }
    
    @Override
    public Pet find(Long id) {
        if (id == null) {
            IllegalArgumentException e = new IllegalArgumentException("Id cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        if (id < 0) {
            IllegalArgumentException e = new IllegalArgumentException("Id cannot be negative");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        Pet pet = getEntityManager().find(Pet.class, id);
        logger.debug("Pet with id={} fetched from DB: {}", id, pet);
        
        return pet;
    }

    @Override
    public void update(Pet pet) {
        if (pet == null) {
            IllegalArgumentException e = new IllegalArgumentException("Pet cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (pet.getId() == null || getEntityManager().find(Pet.class, pet.getId()) == null) {
            IllegalArgumentException e = new IllegalArgumentException("Pet not found in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        logger.debug("Updating pet: ", pet);
        getEntityManager().merge(pet);
    }

    @Override
    public void delete(Pet pet) {
        if (pet == null) {
            IllegalArgumentException e = new IllegalArgumentException("Pet cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (pet.getId() == null || getEntityManager().find(Pet.class, pet.getId()) == null) {
            IllegalArgumentException e = new IllegalArgumentException("Pet not found in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        Pet managedPet = getEntityManager().find(Pet.class, pet.getId());

        logger.debug("Removing pet from DB: {}", pet);
        getEntityManager().remove(managedPet);
    }

    @Override
    public List<Pet> findAll() {
        List<Pet> list = getEntityManager().createQuery("from Pet").getResultList();
        logger.debug("Getting all pets from DB");
        
        return list;
    }

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param entityManager the entityManager to set
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
