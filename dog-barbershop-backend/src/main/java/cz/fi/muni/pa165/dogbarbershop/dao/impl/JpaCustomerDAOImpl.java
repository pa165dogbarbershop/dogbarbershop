package cz.fi.muni.pa165.dogbarbershop.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.fi.muni.pa165.dogbarbershop.dao.CustomerDAO;
import cz.fi.muni.pa165.dogbarbershop.entity.Customer;


/**
 * Implements {@link CustomerDAO} interface for accessing Customer
 * objects. The implementation uses JPA for accessing customer entities
 * in the database.
 *
 * @author David Balcak
 */
public class JpaCustomerDAOImpl implements CustomerDAO {

	static final Logger logger = LoggerFactory.getLogger(JpaCustomerDAOImpl.class);
	
    private EntityManager entityManager;
    
    @Override
    public void create(Customer customer) {
        if (customer == null) {
        	IllegalArgumentException e = new IllegalArgumentException("Customer cannot be null");
        	logger.warn(e.getMessage(), e);
            throw e;
        }

        if (customer.getId() != null && customer.equals(getEntityManager().find(Customer.class, customer.getId()))) {
            IllegalArgumentException e =  new IllegalArgumentException(customer.toString() + " already in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        logger.debug("Trying to persist customer {}", customer);
        getEntityManager().persist(customer);
    }
    
    @Override
    public Customer find(Long id) {
        if (id == null) {
            IllegalArgumentException e = new IllegalArgumentException("Id cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        if (id < 0) {
        	IllegalArgumentException e = new IllegalArgumentException("Id cannot be negative");
        	logger.warn(e.getMessage(), e);
        	throw e;
        }

        Customer customer = getEntityManager().find(Customer.class, id);
        logger.debug("Customer with id={} fetched from database: {}", id, customer);
        return customer;
    }
    
    @Override
    public void update(Customer customer) {
        if (customer == null) {
            IllegalArgumentException e = new IllegalArgumentException("Customer cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (customer.getId() == null || getEntityManager().find(Customer.class, customer.getId()) == null) {
            IllegalArgumentException e = new IllegalArgumentException("Customer not found in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        Customer c = getEntityManager().find(Customer.class, customer.getId());
        if(c != null && c.getAccount() != null && customer.getAccount().getPassword().isEmpty()) {
            customer.getAccount().setPassword(c.getAccount().getPassword());
        }
        
        logger.debug("Updating customer {}", customer);
        getEntityManager().merge(customer);
    }

    @Override
    public void delete(Customer customer) {
        if (customer == null) {
            IllegalArgumentException e = new IllegalArgumentException("Customer cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (customer.getId() == null || getEntityManager().find(Customer.class, customer.getId()) == null) {
            IllegalArgumentException e = new IllegalArgumentException("Customer not found in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        Customer managedCustomer = getEntityManager().find(Customer.class, customer.getId());
        
        logger.debug("Deleting customer: {}", customer);

        getEntityManager().remove(managedCustomer);
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> list = getEntityManager().createQuery("from Customer").getResultList();
        
        logger.debug("Getting all customers from DB");
        
        return list;
    }

    /**
     * @return the em
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param em the em to set
     */
    public void setEntityManager(EntityManager em) {
        this.entityManager = em;
    }
}
