package cz.fi.muni.pa165.dogbarbershop.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * Embeddable object for storing address
 *
 * @author David Balcak
 */
@Embeddable
public class Address implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String street;
    private String zip;
    private String city;
    
    public Address() {
    }

    /**
     * Returns street of the address
     *
     * @return Street
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets street for the address
     *
     * @param street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Returns ZIP number of the address
     *
     * @return ZIP number
     */
    public String getZip() {
        return zip;
    }

    /**
     * Sets ZIP number for the address
     *
     * @param zip
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * Returns city of the address
     *
     * @return City
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets city for the address
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (this.street != null ? this.street.hashCode() : 0);
        hash = 41 * hash + (this.zip != null ? this.zip.hashCode() : 0);
        hash = 41 * hash + (this.city != null ? this.city.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Address other = (Address) obj;
        if ((this.street == null) ? (other.street != null) : !this.street.equals(other.street)) {
            return false;
        }
        if ((this.zip == null) ? (other.zip != null) : !this.zip.equals(other.zip)) {
            return false;
        }
        if ((this.city == null) ? (other.city != null) : !this.city.equals(other.city)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{street: " + street + ", city:" + city + ", ZIP:" + zip + "}";
    }
}
