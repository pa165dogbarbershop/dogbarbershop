package cz.fi.muni.pa165.dogbarbershop.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.stereotype.Service;

import cz.fi.muni.pa165.dogbarbershop.dao.CustomerDAO;
import cz.fi.muni.pa165.dogbarbershop.dto.CustomerDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.PersonDTO;
import cz.fi.muni.pa165.dogbarbershop.entity.Customer;
import cz.fi.muni.pa165.dogbarbershop.service.CustomerService;
import org.dozer.MappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.transaction.annotation.Transactional;

/**
 * Implements CustomerService. Mapping between Entity and its DTO is
 * done using Dozer.
 * 
 * @author Jakub Knetl
 *
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	private CustomerDAO customerDAO;
	private Mapper mapper;
        
        static private final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Override
        @Transactional
	public void create(CustomerDTO customerDTO) {
		if (customerDTO == null){
			throw new IllegalArgumentException("Customer is null");
		}
		
		Customer customer = null;
                try {
                    customer = mapper.map(customerDTO, Customer.class);
                } catch(MappingException ex) {
                    logger.error("Could not map entity "+customerDTO);
                }
		customerDAO.create(customer);
	}

	@Override
        @Transactional
	public void update(CustomerDTO customerDTO) {
		if (customerDTO == null){
			throw new IllegalArgumentException("Customer is null");
		}
		
		Customer customer = null;
                try {
                    customer = mapper.map(customerDTO, Customer.class);
                } catch(MappingException ex) {
                    logger.error("Could not map entity "+customerDTO);
                }
		customerDAO.update(customer);
	}

	@Override
        @Transactional
	public void delete(CustomerDTO customerDTO) {
		if (customerDTO == null){
			throw new IllegalArgumentException("Customer is null");
		}
                
		Customer customer = null;
                try {
                    customer = mapper.map(customerDTO, Customer.class);
                } catch(MappingException ex) {
                    logger.error("Could not map entity "+customerDTO);
                }
		customerDAO.delete(customer);
	}

	@Override
        @Transactional
	public CustomerDTO find(Long id) {
		if (id == null){
			throw new IllegalArgumentException("ID is null");
		}

		Customer customer = customerDAO.find(id);
		CustomerDTO customerDTO = null;
            
                try {
                    customerDTO = mapper.map(customer, CustomerDTO.class);
                } catch(MappingException ex) {
                    logger.error("Could not map entity with 'id="+id+"'");
                }
		
		return customerDTO;
	}

	@Override
        @Transactional
	public List<CustomerDTO> findAll() {
		List<Customer> customers = customerDAO.findAll();
		List<CustomerDTO> customersDTO = new ArrayList<CustomerDTO>();
		
		for (Customer c : customers){
			CustomerDTO customerDTO = null;
            
                        try {
                            customerDTO = mapper.map(c, CustomerDTO.class);
                        } catch(MappingException ex) {
                            logger.error("Could not map entity "+c);
                        }
                        
                        if(customerDTO != null)
                            customersDTO.add(customerDTO);
		}
		
		return customersDTO;
	}
        
        @Override
        public CustomerDTO personToCustomer(PersonDTO person) {
            List<CustomerDTO> customers = findAll();
            
            for(CustomerDTO customer : customers) {
                if(customer.getId() == person.getId())
                    return customer;
            }
            
            return null;
        }

	public CustomerDAO getCustomerDAO() {
		return customerDAO;
	}

	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	public Mapper getMapper() {
		return mapper;
	}

	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	

}
