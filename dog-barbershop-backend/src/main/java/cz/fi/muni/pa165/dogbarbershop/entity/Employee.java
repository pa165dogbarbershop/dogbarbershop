package cz.fi.muni.pa165.dogbarbershop.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Entity representing Employee. Inherits all the attributes
 * of Person and adds employee salary.
 *
 * @author David Balcak
 */
@Entity
public class Employee extends Person implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Column
    private double salary;
    
    public Employee() {
        super();
    }

    /**
     * Returns employee salary
     * 
     * @return Employee salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * Sets employee salary
     * 
     * @param salary Employee salary
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return super.toString() + ", salary=" + salary + "]";
    }
}
