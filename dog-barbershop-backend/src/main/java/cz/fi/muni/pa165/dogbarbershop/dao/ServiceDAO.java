package cz.fi.muni.pa165.dogbarbershop.dao;

import cz.fi.muni.pa165.dogbarbershop.entity.Service;

import java.util.List;

/**
 * ServiceDAO is the interface for accessing Service objects
 *
 * @author David Balcak
 */
public interface ServiceDAO {
    
    /**
     * Creates a new service in the database
     * 
     * @param service 
     * @throws IllegalArgumentException if service is null
     * @throws IllegalArgumentException if service is already in DB
     */
    public void create(Service service);
    
    /**
     * Finds and returns service with given id in the database,
     * otherwise returns null
     * 
     * @param id of the service to find
     * @return Found service or null if not found
     * @throws IllegalArgumentException if id is negative or null
     */
    public Service find(Long id);
    
    /**
     * Updates existing service in the database
     * 
     * @param service
     * @throws IllegalArgumentException if service is null
     * @throws IllegalArgumentException if service is not in DB
     */
    public void update(Service service);
    
    /**
     * Deletes service from the database
     * 
     * @param service 
     * @throws IllegalArgumentException if service is null
     * @throws IllegalArgumentException if service is not in DB
     */
    public void delete(Service service);
    
    
    /**
     * Finds and returns all the services in the database, or returns
     * empty list if the database is empty
     * 
     * @return List of services
     */
    public List<Service> findAll();
}
