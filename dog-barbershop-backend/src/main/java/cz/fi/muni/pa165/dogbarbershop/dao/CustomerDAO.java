package cz.fi.muni.pa165.dogbarbershop.dao;

import cz.fi.muni.pa165.dogbarbershop.entity.Customer;

import java.util.List;

/**
 * CustomerDAO is the interface for accessing Customer objects
 *
 * @author Petr Baláš
 */
public interface CustomerDAO {

    /**
     * Creates a new Customer in the database
     * 
     * @param customer to be created
     * @throws IllegalArgumentException if customer is null
     * @throws IllegalArgumentException if customer is already in DB
     */
    public void create(Customer customer);
    
    /**
     * Finds and returns customer with given id in the database,
     * otherwise returns null
     * 
     * @param id of the customer to find
     * @return Found customer or null if not found
     * @throws IllegalArgumentException if id is negative or null
     */
    public Customer find(Long id);
    
    /**
     * Updates existing customer in the database
     * 
     * @param customer
     * @throws IllegalArgumentException if customer is null
     * @throws IllegalArgumentException if customer is not in DB
     */
    public void update(Customer customer);
    
    /**
     * Deletes customer from the database
     * 
     * @param customer 
     * @throws IllegalArgumentException if customer is null
     * @throws IllegalArgumentException if customer is not in DB
     */
    public void delete(Customer customer);
    
    /**
     * Finds and returns all the customers in the database, or returns
     * empty list if the database is empty
     * 
     * @return List of customers
     */
    public List<Customer> findAll();
}
