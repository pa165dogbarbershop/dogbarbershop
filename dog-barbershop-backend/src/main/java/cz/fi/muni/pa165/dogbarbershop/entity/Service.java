package cz.fi.muni.pa165.dogbarbershop.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Entity representing Service.
 *
 * @author Petr Baláš
 */
@Entity
public class Service implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(nullable = false)
    private String name;
    
    @Column
    private int duration;
    
    @Column
    private double price;
    
    @ManyToOne(fetch=FetchType.EAGER)
    private Pet pet;

    /**
     * Returns service id
     *
     * @return Id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets service id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns service name
     *
     * @return Name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets service name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns duration of this service
     * 
     * @return Duration in seconds
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Sets duration for this service
     * 
     * @param duration Duration in seconds
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Returns price of this service
     * 
     * @return Price
     */
    public double getPrice() {
        return price;
    }

    /**
     * Sets price for this service
     * 
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Returns pet of this service
     * 
     * @return Pet
     */
    public Pet getPet() {
        return pet;
    }

    /**
     * Sets pet for this service
     * 
     * @param pet
     */
    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Service other = (Service) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        String service = "Service [id=" + id + ", name=" + name
                + ", duration=" + duration + ", price=" + price;
        if (pet != null) {
            service += ", pet=" + pet.getName();
        }
        return service + "]";
    }
}
