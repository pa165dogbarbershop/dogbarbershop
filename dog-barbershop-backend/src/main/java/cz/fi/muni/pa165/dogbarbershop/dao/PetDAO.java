package cz.fi.muni.pa165.dogbarbershop.dao;

import cz.fi.muni.pa165.dogbarbershop.entity.Pet;

import java.util.List;

/**
 * PetDAO is the interface for accessing Pet objects
 *
 * @author David Balcak
 */
public interface PetDAO {
    
    /**
     * Creates a new pet in the database
     * 
     * @param pet 
     * @throws IllegalArgumentException if pet is null
     * @throws IllegalArgumentException if pet is already in DB
     */
    public void create(Pet pet);
    
    /**
     * Finds and returns pet with given id in the database,
     * otherwise returns null
     * 
     * @param id of the pet to find
     * @return Found pet or null if not found
     * @throws IllegalArgumentException if id is negative or null
     */
    public Pet find(Long id);
    
    /**
     * Updates existing pet in the database
     * 
     * @param pet
     * @throws IllegalArgumentException if pet is null
     * @throws IllegalArgumentException if pet is not in DB
     */
    public void update(Pet pet);
    
    /**
     * Deletes pet from the database
     * 
     * @param pet 
     * @throws IllegalArgumentException if pet is null
     * @throws IllegalArgumentException if pet is not in DB
     */
    public void delete(Pet pet);
    
    
    /**
     * Finds and returns all the pets in the database, or returns
     * empty list if the database is empty
     * 
     * @return List of pets
     */
    public List<Pet> findAll();
}
