package cz.fi.muni.pa165.dogbarbershop.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.fi.muni.pa165.dogbarbershop.dao.EmployeeDAO;
import static cz.fi.muni.pa165.dogbarbershop.dao.impl.JpaCustomerDAOImpl.logger;
import cz.fi.muni.pa165.dogbarbershop.entity.Employee;

/**
 * Implements {@link EmployeeDAO} interface for accessing Employee
 * objects. The implementation uses JPA for accessing employee entities
 * in the database.
 *
 * @author David Balack
 */
public class JpaEmployeeDAOImpl implements EmployeeDAO {
	
	static final Logger logger = LoggerFactory.getLogger(JpaEmployeeDAOImpl.class);
    
    private EntityManager entityManager;

    @Override
    public void create(Employee employee) {
        if (employee == null) {
            IllegalArgumentException e = new IllegalArgumentException("Employee cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (employee.getId() != null && employee.equals(getEntityManager().find(Employee.class, employee.getId()))) {
            IllegalArgumentException e = new IllegalArgumentException(employee.toString() + " already in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        logger.debug("Storing employee into DB: {}", employee);
        getEntityManager().persist(employee);
    }
    
    @Override
    public Employee find(Long id) {
        if (id == null) {
            IllegalArgumentException e = new IllegalArgumentException("Id cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        if (id < 0) {
            IllegalArgumentException e = new IllegalArgumentException("Id cannot be negative");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        Employee employee = getEntityManager().find(Employee.class, id);
        logger.debug("Employee with id={} fetched from DB with value: ", id, employee);
        
        return employee;
    }

    @Override
    public void update(Employee employee) {
        if (employee == null) {
            IllegalArgumentException e = new IllegalArgumentException("Employee cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (employee.getId() == null || getEntityManager().find(Employee.class, employee.getId()) == null) {
            IllegalArgumentException e = new IllegalArgumentException("Employee not found in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        
        Employee e = getEntityManager().find(Employee.class, employee.getId());
        if(e != null && e.getAccount() != null && employee.getAccount().getPassword().isEmpty()) {
            employee.getAccount().setPassword(e.getAccount().getPassword());
        }
        
        logger.debug("Updating employee {}", employee);
        getEntityManager().merge(employee);
    }

    @Override
    public void delete(Employee employee) {
        if (employee == null) {
            IllegalArgumentException e = new IllegalArgumentException("Employee cannot be null");
            logger.warn(e.getMessage(), e);
            throw e;
        }

        if (employee.getId() == null || getEntityManager().find(Employee.class, employee.getId()) == null) {
            IllegalArgumentException e = new IllegalArgumentException("Employee not found in DB");
            logger.warn(e.getMessage(), e);
            throw e;
        }
        Employee managedEmployee = getEntityManager().find(Employee.class, employee.getId());
        getEntityManager().remove(managedEmployee);
    }

    @Override
    public List<Employee> findAll() {
        List<Employee> list = getEntityManager().createQuery("from Employee").getResultList();

        return list;
    }

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param entityManager the entityManager to set
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
