/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.pa165.dogbarbershop.service.impl;

import cz.fi.muni.pa165.dogbarbershop.dao.PetDAO;
import cz.fi.muni.pa165.dogbarbershop.dto.PetDTO;
import cz.fi.muni.pa165.dogbarbershop.entity.Employee;
import cz.fi.muni.pa165.dogbarbershop.entity.Pet;
import cz.fi.muni.pa165.dogbarbershop.service.PetService;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.dozer.MappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Petr Baláš
 */
@Service
@Transactional
public class PetServiceImpl implements PetService {
    private PetDAO petDAO;
    private Mapper mapper;
    
    static private final Logger logger = LoggerFactory.getLogger(PetServiceImpl.class);

    @Override
    @Transactional
    public void create(PetDTO petDTO) {
            if (petDTO == null){
                    throw new IllegalArgumentException("Pet is null");
            }

            Pet pet = null;
            try {
                pet = mapper.map(petDTO, Pet.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+petDTO);
            }
            petDAO.create(pet);
    }

    @Override
    @Transactional
    public void update(PetDTO petDTO) {
            if (petDTO == null){
                    throw new IllegalArgumentException("Pet is null");
            }

            Pet pet = null;
            try {
                pet = mapper.map(petDTO, Pet.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+petDTO);
            }
            petDAO.update(pet);
    }

    @Override
    @Transactional
    public void delete(PetDTO petDTO) {
            if (petDTO == null){
                    throw new IllegalArgumentException("Pet is null");
            }

            Pet pet = null;
            try {
                pet = mapper.map(petDTO, Pet.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity "+petDTO);
            }
            petDAO.delete(pet);
    }

    @Override
    @Transactional
    public PetDTO find(Long id) {
            if (id == null){
                    throw new IllegalArgumentException("ID is null");
            }

            
            Pet pet = petDAO.find(id);
            PetDTO petDTO = null;
            try {
                petDTO = mapper.map(pet, PetDTO.class);
            } catch(MappingException ex) {
                logger.error("Could not map entity with 'id="+id+"'");
            }

            return petDTO;
    }

    @Override
    @Transactional
    public List<PetDTO> findAll() {
            List<Pet> pets = petDAO.findAll();
            List<PetDTO> petsDTO = new ArrayList<PetDTO>();

            for (Pet p : pets){
                    PetDTO petDTO = null;
            
                    try {
                        petDTO = mapper.map(p, PetDTO.class);
                    } catch(MappingException ex) {
                        logger.error("Could not map entity "+p);
                    }
                    
                    if(petDTO != null)
                        petsDTO.add(petDTO);
            }

            return petsDTO;
    }

    public PetDAO getPetDAO() {
            return petDAO;
    }

    public void setPetDAO(PetDAO petDAO) {
            this.petDAO = petDAO;
    }

    public Mapper getMapper() {
            return mapper;
    }

    public void setMapper(Mapper mapper) {
            this.mapper = mapper;
    }
}
