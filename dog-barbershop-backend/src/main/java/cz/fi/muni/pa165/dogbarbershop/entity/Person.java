package cz.fi.muni.pa165.dogbarbershop.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Abstract entity. Represents a person with firstName, lastName,
 * email, address and phone.
 *
 * @author David Balcak
 */
@Entity
public abstract class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String email;

    @OneToOne(cascade = CascadeType.ALL)
    private Account account;

    @Embedded
    private Address address;

    private String phone;

    public Person() {
    }

    /**
     * Returns id of this entity
     *
     * @return Id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id for this entity
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns first name of this entity
     *
     * @return First Name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name for this entity
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Returns last name of this entity
     *
     * @return Last Name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name for this entity
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns email of this entity
     *
     * @return Email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email for this entity
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Returns address of this entity
     *
     * @return Address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets address for this entity
     *
     * @param address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Returns phone number of this entity
     *
     * @return Phone Number
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets phone number for this entity
     *
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Person [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
            + ", account=" + account + ", address=" + address + ", phone=" + phone + "]";
    }



}
