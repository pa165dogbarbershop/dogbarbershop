package cz.fi.muni.pa165.dogbarbershop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class LoggingAspect {

    static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    // @Before("execution(public * cz.fi.muni.pa165.dogbarbershop.service.impl.*.*(..))")
    @Before("execution(public * cz.fi.muni.dogbarbershop.web.controller.*.*(..)) ||"
            + " execution(public * cz.fi.muni.pa165.dogbarbershop.service.impl.*.*(..)) ||"
            + " execution(public * cz.fi.muni.dogbarbershop.web.ws.*.*(..))")
    public void logMethod(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        Object[] args = joinPoint.getArgs();
        StringBuilder builder = new StringBuilder();
        builder.append(signature.getDeclaringTypeName() + "#");
        builder.append(signature.getName() + " called.");
        if (args.length > 0) {
            builder.append("Parameters: [");
            for (int i = 0; i < args.length; i++) {
                if (i != 0)
                    builder.append(", ");
                builder.append(args[i].toString());
            }
            builder.append("]");
        }
        logger.debug(builder.toString());

    }

}
