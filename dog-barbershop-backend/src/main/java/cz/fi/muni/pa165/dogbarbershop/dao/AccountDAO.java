/**
 *
 */
package cz.fi.muni.pa165.dogbarbershop.dao;

import java.util.List;

import cz.fi.muni.pa165.dogbarbershop.entity.Account;

/**
 * @author jknetl
 *
 */
public interface AccountDAO {

    /**
     * Creates a new Account in the database
     *
     * @param Account to be created
     * @throws IllegalArgumentException if Account is null
     * @throws IllegalArgumentException if Account is already in DB
     */
    public void create(Account account);

    /**
     * Finds and returns Account with given id in the database,
     * otherwise returns null
     *
     * @param id of the Account to find
     * @return Found Account or null if not found
     * @throws IllegalArgumentException if id is negative or null
     */
    public Account find(Long id);
    
    /**
     * Finds and returns Account with given login in the database,
     * otherwise returns null
     *
     * @param login of the Account to find
     * @return Found Account or null if not found
     * @throws IllegalArgumentException if id is negative or null
     */
    public Account find(String login);

    /**
     * Updates existing Account in the database
     *
     * @param Account
     * @throws IllegalArgumentException if Account is null
     * @throws IllegalArgumentException if Account is not in DB
     */
    public void update(Account account);

    /**
     * Deletes Account from the database
     *
     * @param Account
     * @throws IllegalArgumentException if Account is null
     * @throws IllegalArgumentException if Account is not in DB
     */
    public void delete(Account account);

    /**
     * Finds and returns all the Accounts in the database, or returns
     * empty list if the database is empty
     *
     * @return List of Accounts
     */
    public List<Account> findAll();
}
