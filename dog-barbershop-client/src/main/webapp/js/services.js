(function() {
  var app = angular.module('services', []);

  app.controller('ServiceController', ['$http', '$scope', function($http, $scope) {
    var controller = this;
    controller.services = [];

    $http.get(baseApiUrl + '/customers/'+customerId+'/services').success(function(data) {
      controller.services = data;
    });

    this.formTab = 1;
    this.service = {};

    this.addService = function() {
      $http.post(baseApiUrl + '/services', this.service).success(function(data) {
        controller.services.push(data);
      });;

      this.service = {};
      $scope.serviceForm.$setPristine();
    };

    this.editService = function() {
      $http.patch(baseApiUrl + '/services/' + this.service.id, this.service);

      this.service = {};
      $scope.serviceForm.$setPristine();
      this.formTab = 1;
    };

    this.removeService = function(removedService) {
      $http.delete(baseApiUrl + '/services/' + removedService.id).success(function(data) {
        controller.services.splice(controller.services.indexOf(removedService), 1);
      });
    };

    this.formTabSet = function(checkTab) {
      return this.formTab === checkTab;
    };

    this.loadEditForm = function(service) {
      this.origService = angular.copy(service);
      this.service = service;
      this.formTab = 2;
    };

    this.cancelEditForm = function() {
      this.service.name = this.origService.name;
      this.service.duration = this.origService.duration;
      this.service.price = this.origService.price;
      this.service.pet = this.origService.pet;
      this.origService = {};
      this.service = {};
      $scope.serviceForm.$setPristine();
      this.formTab = 1;
    };
  }]);

  app.directive('tabServices', function() {
    return {
      restrict: "E",
      templateUrl: "views/tab-services.html"
    };
  });

  app.directive('addService', function() {
    return {
      restrict: "E",
      templateUrl: "views/add-service.html",
      controller: function($http, $scope) {
        $scope.pets = [];

        $http.get(baseApiUrl + '/customers/'+customerId+'/pets').success(function(data) {
          $scope.pets = data;
        });
      }
    };
  });

  app.directive('editService', function() {
    return {
      restrict: "E",
      templateUrl: "views/edit-service.html",
      controller: function($http, $scope) {
        $scope.pets = [];

        $http.get(baseApiUrl + '/customers/'+customerId+'/pets').success(function(data) {
          $scope.pets = data;
        });
      }
    };
  });
})();
