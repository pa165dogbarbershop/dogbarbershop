(function() {
  var app = angular.module('pets', []);

  app.controller('PetController', ['$http', '$scope', function($http, $scope) {
    var controller = this;
    controller.pets = [];
    
    $http.get(baseApiUrl + '/customers/'+customerId+'/pets').success(function(data) {
      controller.pets = data;
    });

    this.formTab = 1;
    this.pet = {};

    this.addPet = function() {
      this.pet.services = [];
      this.pet.customer = customer;

      $http.post(baseApiUrl + '/pets', this.pet).success(function(data) {
        controller.pets.push(data);
      });

      this.pet = {};
      $scope.petForm.$setPristine();
    };

    this.editPet = function() {
      this.pet.customer = customer;
        
      $http.patch(baseApiUrl + '/pets/' + this.pet.id, this.pet);

      this.pet = {};
      $scope.petForm.$setPristine();
      this.formTab = 1;
    };

    this.removePet = function(removedPet) {
      $http.delete(baseApiUrl + '/pets/' + removedPet.id).success(function(data, status) {
        controller.pets.splice(controller.pets.indexOf(removedPet), 1);
      });
    };

    this.formTabSet = function(checkTab) {
      return this.formTab === checkTab;
    };

    this.loadEditForm = function(pet) {
      this.origPet = angular.copy(pet);
      this.pet = pet;
      this.pet.birthDate = new Date(pet.birthDate);
      this.formTab = 2;
    };

    this.cancelEditForm = function() {
      this.pet.name = this.origPet.name;
      this.pet.breed = this.origPet.breed;
      this.pet.birthDate = this.origPet.birthDate;
      this.pet.fur = this.origPet.fur;
      this.origPet = {};
      this.pet = {};
      $scope.petForm.$setPristine();
      this.formTab = 1;
    };
  }]);

  app.directive('tabPets', function() {
    return {
      restrict: "E",
      templateUrl: "views/tab-pets.html"
    };
  });

  app.directive('addPet', function() {
    return {
      restrict: "E",
      templateUrl: "views/add-pet.html"
    };
  });

  app.directive('editPet', function() {
    return {
      restrict: "E",
      templateUrl: "views/edit-pet.html"
    };
  });
})();
