var baseApiUrl = 'http://localhost:8080/pa165/ws';
var customerId = 1;
var customer = {};

(function() {
  var app = angular.module('dogBarbershop', ['pets', 'services']);

  app.controller('BarbershopController', ['$http', '$scope', function($http, $scope) {
    var controller = this;
    this.apiUrl = baseApiUrl;
    $scope.customerId = customerId;

    $http.get(baseApiUrl + '/customers/'+customerId).success(function(data, status) {
      if (status === 200) {
          delete controller.hasError;
          customer = data;
          $scope.customerName = customer.fullName;
      }
      controller.tab = 1;
    }).error(function(data) {

      controller.hasError = true;
      controller.tab = 0;
    });

    this.tabSet = function(checkTab) {
      return this.tab === checkTab;
    };

    this.setTab = function(activeTab) {
      this.tab = activeTab;
    };
  }]);

  app.filter('titlecase', function() {
    return function(s) {
        s = ( s === undefined || s === null ) ? '' : s;
        return s.toString().toLowerCase().replace( /\b([a-z])/g, function(ch) {
            return ch.toUpperCase();
        });
    };
  });

})();
