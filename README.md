# README #

Dog barbershop is web-app for managing barbershop. You can manage using web frontend:
* customers
* employees
* pets
* services

The application also have REST API implemented so you can manage using REST:
* pets
* services

### How to run application ###

The whole build and run process is managed via maven, so you need to have maven installed in your system. As data source the applications assumes that there is derby database running on port 1527 (both user and password is pa165).

Let $(BARBERSHOP_FOLDER) is folder where you cloned this repo. Then:

* Change directory to **$(BARBERSHOP_FOLDER)**
* Build it using:
```sh
$ mvn clean install
```
* Then you can run web frontend. Change directory to **$(BARBERSHOP_FOLDER)/dog-barbershop-frontend/** and run it using command: 
```sh
$ mvn tomcat7:run
```
(note: we do not recommend to use java 8 for execution. Tomcat 7 has issues with running on java 8.

* To run web rest client first make sure that web frontend is running (it contains REST API). Then change directory to **$(BARBERSHOP_FOLDER)/dog-barbershop-client/** and run it using command:
```sh
$ mvn jetty:run
```