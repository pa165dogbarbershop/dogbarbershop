package cz.fi.muni.dogbarbershop.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author David Balcak
 */

@Controller
public class BaseController {

    @ModelAttribute("controllerName")
    public String controllerName() {
        String name = this.getClass().getSimpleName();
        return name.substring(0, name.length() - 10).toLowerCase();
    }
}
