package cz.fi.muni.dogbarbershop.web.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SecurityInitializer
    extends AbstractSecurityWebApplicationInitializer {
}