/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.dogbarbershop.web.converters;

import cz.fi.muni.pa165.dogbarbershop.dto.CustomerDTO;
import cz.fi.muni.pa165.dogbarbershop.service.CustomerService;
import java.beans.PropertyEditorSupport;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Baly
 */
public class CustomerEditor extends PropertyEditorSupport {
    
    private CustomerService customerService;
    
    public CustomerEditor(CustomerService service) {
        this.customerService = service;
    }

    // Converts a String to a CustomerDTO (when submitting form)
    @Override
    public void setAsText(String text) {
        CustomerDTO c = customerService.find(Long.parseLong(text));
        this.setValue(c);
    }
    
}
