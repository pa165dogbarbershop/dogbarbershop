package cz.fi.muni.dogbarbershop.web.validation;

import cz.fi.muni.pa165.dogbarbershop.dto.PetDTO;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Petr Balas
 */
public class PetSpringValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return PetDTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PetDTO pet = (PetDTO) target;
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "breed", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthDate", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fur", "required");
        
        if (pet.getName().length() > 30) {
            errors.rejectValue("name", "tooLong");
        }
        
        if(!pet.getBreed().matches(ValidationRegex.ONLY_LETTERS.getPattern())) {
            errors.rejectValue("breed", "notOnlyLetters");
        }
        
        if(pet.getCustomer() == null) {
            errors.rejectValue("customer", "mustHaveValue");
        }
    }
}
