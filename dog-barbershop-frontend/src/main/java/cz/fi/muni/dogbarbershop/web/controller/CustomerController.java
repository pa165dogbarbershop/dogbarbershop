package cz.fi.muni.dogbarbershop.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import cz.fi.muni.dogbarbershop.web.validation.CustomerSpringValidator;
import cz.fi.muni.pa165.dogbarbershop.dto.AccountDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.CustomerDTO;
import cz.fi.muni.pa165.dogbarbershop.service.AccountService;
import cz.fi.muni.pa165.dogbarbershop.service.CustomerService;

/**
 * Customer controller.
 *
 * @author Jakub Knetl
 *
 */

@Controller
@Secured ({"ROLE_ADMIN", "ROLE_EMPLOYEE"})
@RequestMapping("/customer")
public class CustomerController extends BaseController {

	@Autowired
	private CustomerService customerService;

        @Autowired
	private MessageSource messageSource;

        @Autowired
	private AccountService accountService;

	@ModelAttribute("customers")
	public List<CustomerDTO> allCustomers(){
            List<CustomerDTO> customers = customerService.findAll();
            AccountDTO loggedAccount = accountService.getLoggedAccount();
            CustomerDTO customer = null;
            if (loggedAccount != null){
                customer = customerService.personToCustomer(accountService.getLoggedAccount().getPerson());
            }

            if(!accountService.hasRole("ROLE_ADMIN") && customer != null) {
                customers = new ArrayList();
                customers.add(customer);
            }

            return customers;
	}

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String view(@PathVariable Long id, Model model) {
        model.addAttribute("customer", customerService.find(id));
        return "customer/view";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("customers", allCustomers());
        return "customer/list";
    }

    @RequestMapping(value = "/{id}/pets", method = RequestMethod.GET)
    public String pets(@PathVariable Long id, Model model) {
        model.addAttribute("customer", customerService.find(id));
        model.addAttribute("customerPets", customerService.find(id).getPets());
        return "customer/pets";
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
    public String delete(@PathVariable Long id, RedirectAttributes redirectAttributes, Locale locale, UriComponentsBuilder uriBuilder) {
        CustomerDTO customer = customerService.find(id);
        customerService.delete(customer);
        redirectAttributes.addFlashAttribute(
                "message",
                messageSource.getMessage("customerDTO.delete.message", new Object[]{customer.getFirstName(), customer.getLastName(), customer.getId()}, locale)
        );
        return "redirect:" + uriBuilder.path("/customer/list").build();
    }

    @RequestMapping(value = "/{id}/update", method = RequestMethod.GET)
    public String update_form(@PathVariable Long id, Model model) {
        CustomerDTO customer = customerService.find(id);
        model.addAttribute("customer", customer);

        return "customer/edit";
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(new CustomerSpringValidator());
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("customer") CustomerDTO customer, BindingResult bindingResult, RedirectAttributes redirectAttributes, UriComponentsBuilder uriBuilder, Locale locale) {

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (bindingResult.hasErrors()) {
            return (customer.getId() == null)?"customer/add":"customer/edit";
        }

        if (customer.getId() == null) {
            AccountDTO account = customer.getAccount();
            account.setPassword(passwordEncoder.encode(account.getPassword()));

            customerService.create(customer);
            redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("customerDTO.add.message", new Object[]{customer.getFirstName(), customer.getLastName(), customer.getId()}, locale)
            );
        } else {
            if (customer.getAccount().getPassword() == null ||
                customer.getAccount().getPassword().isEmpty()){
                CustomerDTO beforeModification = customerService.find(customer.getId());
                customer.getAccount().setPassword(beforeModification.getAccount().getPassword());
            }
            else{
                AccountDTO account = customer.getAccount();
                account.setPassword(passwordEncoder.encode(account.getPassword()));
            }
            customerService.update(customer);
            redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("customerDTO.updated.message", new Object[]{customer.getFirstName(), customer.getLastName(), customer.getId()}, locale)
            );
        }
        return "redirect:" + uriBuilder.path("/customer/list").build();
    }

    @RequestMapping(value="/add", method = RequestMethod.GET)
    public String add(Model model){
    	model.addAttribute("customer", new CustomerDTO());
    	return "customer/add";
    }
}
