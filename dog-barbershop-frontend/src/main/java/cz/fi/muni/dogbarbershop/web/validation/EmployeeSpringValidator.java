package cz.fi.muni.dogbarbershop.web.validation;

import cz.fi.muni.pa165.dogbarbershop.dto.EmployeeDTO;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class EmployeeSpringValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return EmployeeDTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
            EmployeeDTO employee = (EmployeeDTO) target;
            
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "salary", "required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.city", "required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.street", "required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.zip", "required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "account.login", "required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "account.password", "required");
            
            if (employee.getFirstName().length() > 30) {
                errors.rejectValue("firstName", "tooLong");
            }

            if (employee.getLastName().length() > 30) {
                errors.rejectValue("lastName", "tooLong");
            }

            if(!employee.getFirstName().matches(ValidationRegex.ONLY_LETTERS.getPattern())) {
                errors.rejectValue("firstName", "notOnlyLetters");
            }

            if(!employee.getLastName().matches(ValidationRegex.ONLY_LETTERS.getPattern())) {
                errors.rejectValue("lastName", "notOnlyLetters");
            }

            if(!employee.getEmail().matches(ValidationRegex.EMAIL.getPattern())) {
                errors.rejectValue("email", "wrongFormat");
            }

            if(employee.getEmail().length() > 70) {
                errors.rejectValue("email", "tooLong");
            }

            if(!employee.getPhone().matches(ValidationRegex.ONLY_POSITIVE_NUMBERS.getPattern())) {
                errors.rejectValue("phone", "wrongFormat");
            }
            
            if(employee.getPhone().length() < 9) {
                errors.rejectValue("phone", "tooShort");
            }

            if(employee.getAddress().getCity().length() > 40) {
                errors.rejectValue("address.city", "tooLong");
            }

            if(employee.getAddress().getStreet().length() > 40) {
                errors.rejectValue("address.street", "tooLong");
            }

            if(employee.getAddress().getZip().length() > 20) {
                errors.rejectValue("address.zip", "tooLong");
            }

            if(!employee.getAddress().getZip().matches(ValidationRegex.ONLY_POSITIVE_NUMBERS.getPattern())) {
                errors.rejectValue("address.zip", "wrongFormat");
            }
            
            if(employee.getSalary() > 1000000) {
                errors.rejectValue("salary", "tooMuch");
            }
            
            if(employee.getSalary() < 1) {
                errors.rejectValue("salary", "notEnough");
            }
            
            if(!employee.getAccount().getPassword().matches(ValidationRegex.PASSWORD.getPattern())) {
                errors.rejectValue("account.password","wrongFormat");
            }

            if(employee.getAccount().getLogin().length() > 30) {
                errors.rejectValue("account.login","tooLong");
            }
	}

}
