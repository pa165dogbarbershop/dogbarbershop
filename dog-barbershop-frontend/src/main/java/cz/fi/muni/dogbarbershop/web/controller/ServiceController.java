package cz.fi.muni.dogbarbershop.web.controller;

import cz.fi.muni.dogbarbershop.web.converters.PetEditor;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import cz.fi.muni.dogbarbershop.web.validation.ServiceSpringValidator;
import cz.fi.muni.pa165.dogbarbershop.dto.AccountDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.CustomerDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.PetDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.ServiceDTO;
import cz.fi.muni.pa165.dogbarbershop.service.AccountService;
import cz.fi.muni.pa165.dogbarbershop.service.CustomerService;
import cz.fi.muni.pa165.dogbarbershop.service.PetService;
import cz.fi.muni.pa165.dogbarbershop.service.ServiceService;
import java.util.ArrayList;

@Controller
@RequestMapping("/service")
public class ServiceController extends BaseController {
	@Autowired
	private ServiceService serviceService;
        
        @Autowired
	private PetService petService;
        
        @Autowired
	private CustomerService customerService;

        @Autowired
	private AccountService accountService;
        
	@Autowired
	private MessageSource messageSource;
	
	@ModelAttribute("service/list")
	public List<ServiceDTO> allServices(){
		return serviceService.findAll();
	}
        
        @ModelAttribute("petsList")
	public List<PetDTO> allPets(){
		List<PetDTO> pets = petService.findAll();

                AccountDTO loggedAccount = accountService.getLoggedAccount();
                CustomerDTO customer = null;
                if (loggedAccount != null){
                    customer = customerService.personToCustomer(accountService.getLoggedAccount().getPerson());
                }

                if(!accountService.hasRole("ROLE_ADMIN") && customer != null) {
                    pets = new ArrayList(customer.getPets());
                }

                return pets;
	}
	
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String view(@PathVariable Long id, Model model) {
        model.addAttribute("service", serviceService.find(id));
        return "service/view";
    }
        
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        AccountDTO loggedAccount = accountService.getLoggedAccount();

        CustomerDTO customer = null;
        if (loggedAccount != null)
            customer = customerService.personToCustomer(accountService.getLoggedAccount().getPerson());

        if (!accountService.hasRole("ROLE_ADMIN") && customer != null) {
            List<ServiceDTO> services = new ArrayList<>();
            for (PetDTO pet : customer.getPets()) {
                services.addAll(pet.getServices());
            }
            model.addAttribute("services", services);
        } else {
            model.addAttribute("services", allServices());
        }
        return "service/list";
    }
   
    @RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
    public String delete(@PathVariable Long id, RedirectAttributes redirectAttributes, Locale locale, UriComponentsBuilder uriBuilder) {
        ServiceDTO service = serviceService.find(id);;
        serviceService.delete(service);
        redirectAttributes.addFlashAttribute(
                "message",
                messageSource.getMessage("serviceDTO.delete.message", new Object[]{service.getName(), service.getId()}, locale)
        );
        return "redirect:" + uriBuilder.path("/service/list").build();
    }

    @RequestMapping(value = "/{id}/update", method = RequestMethod.GET)
    public String update_form(@PathVariable Long id, Model model) {
        ServiceDTO service = serviceService.find(id);
        model.addAttribute("service", service);

        return "service/edit";
    }
    
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(PetDTO.class, new PetEditor(petService));
        
        binder.addValidators(new ServiceSpringValidator());
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("service") ServiceDTO service, BindingResult bindingResult, RedirectAttributes redirectAttributes, UriComponentsBuilder uriBuilder, Locale locale) {

        if (bindingResult.hasErrors()) {
            return (service.getId()==null)?"service/add":"service/edit";
        }
        if (service.getId() == null) {
            serviceService.create(service);
            redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("serviceDTO.add.message", new Object[]{service.getName(),  service.getId()}, locale)
            );
        } else {
            serviceService.update(service);
            redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("serviceDTO.updated.message", new Object[]{service.getName(),  service.getId()}, locale)
            );
        }
        return "redirect:" + uriBuilder.path("/service/list").build();
    }
    
    @RequestMapping(value="/add", method = RequestMethod.GET)
    public String add(Model model){
    	model.addAttribute("service", new ServiceDTO());
    	return "service/add";
    }
}
