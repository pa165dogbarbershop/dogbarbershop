/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.dogbarbershop.web.ws;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cz.fi.muni.pa165.dogbarbershop.dto.ServiceDTO;
import cz.fi.muni.pa165.dogbarbershop.service.ServiceService;


/**
 *
 * @author Petr Baláš
 */
@RestController
@RequestMapping("/ws/services")
public class ServiceWebService {

	@Autowired
	private ServiceService serviceService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<ServiceDTO> getService(@PathVariable("id") Long id) {
            HttpStatus status = HttpStatus.OK;

            ServiceDTO service = null;
            try {
                service = serviceService.find(id);
            } catch(Exception ex) {
                status = HttpStatus.NOT_FOUND;
            }

            return new ResponseEntity<ServiceDTO>(service ,status);
	}

	@RequestMapping(value = "", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<ServiceDTO>> getAll(){
            HttpStatus status = HttpStatus.OK;

            List<ServiceDTO> services = new ArrayList<ServiceDTO>();

            try {
                services = serviceService.findAll();
            } catch(Exception ex) {
                status = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            return new ResponseEntity<List<ServiceDTO>>(services, status);
	}

	@RequestMapping(value = "", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<ServiceDTO> add(@RequestBody ServiceDTO service){

            HttpStatus status = HttpStatus.CREATED;

            try {
                serviceService.create(service);
            } catch(Exception ex) {
                status = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            return new ResponseEntity<ServiceDTO>(service,status);
	}

        @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, consumes = "application/json", produces = "application/json")
	public ResponseEntity<ServiceDTO> update(@RequestBody ServiceDTO service){
            HttpStatus status = HttpStatus.OK;

            serviceService.update(service);

            ServiceDTO s = serviceService.find(service.getId());

            if(s == null) {
                status = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            return new ResponseEntity<ServiceDTO>(s,status);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
        @ResponseStatus(value = HttpStatus.OK)
	public void delete(@PathVariable("id") Long id){
            ServiceDTO service = serviceService.find(id);
            serviceService.delete(service);
	}

}
