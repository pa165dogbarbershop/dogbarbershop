package cz.fi.muni.dogbarbershop.web.config;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.jsp.jstl.core.Config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 *
 * Initializes spring aplication. It create web application context
 *  and register DispatcherServlet.
 *
 * @author Jakub Knetl
 *
 */
public class SpringInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    static final Logger logger = LoggerFactory.getLogger(SpringInitializer.class);


    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        //Use UTF-8
        FilterRegistration.Dynamic encoding = servletContext.addFilter("encoding", CharacterEncodingFilter.class);
        encoding.setInitParameter("encoding", "utf-8");
        encoding.addMappingForUrlPatterns(null, false, "/*");

        servletContext.setInitParameter(Config.FMT_LOCALIZATION_CONTEXT,"Texts");
        super.onStartup(servletContext);
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] {SecurityConfig.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] {SpringMVCConfig.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] {"/"};
    }
}
