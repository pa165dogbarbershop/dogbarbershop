package cz.fi.muni.dogbarbershop.web.config;

import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


/**
 *
 * Programatically specified spring mvc configuration.
 *
 * @author Jakub Knetl
 *
 */
@EnableWebMvc
@Configuration
@ComponentScan(basePackages = {"cz.fi.muni.dogbarbershop.web.controller", "cz.fi.muni.dogbarbershop.web.config",
    "cz.fi.muni.dogbarbershop.web.ws"})
@ImportResource({ "classpath:service-context.xml" })
public class SpringMVCConfig extends WebMvcConfigurerAdapter {

    static final Logger logger = LoggerFactory.getLogger(SpringMVCConfig.class);

    /**
     * Map views to /WEB-INF/views/ directory with suffix jsp
     *
     * @return
     */
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

        /**
     * Provides localized messages.
     */
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("Texts");
        return messageSource;
    }

    /**
     * Provides JSR-303 Validator.
     */
    @Bean
    public Validator validator() {
        return new LocalValidatorFactoryBean();
    }

    /**
     * Provides static resources
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/**");
        super.addResourceHandlers(registry);
    }
}
