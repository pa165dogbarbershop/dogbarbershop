/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.dogbarbershop.web.controller;

import cz.fi.muni.pa165.dogbarbershop.dto.AccountDTO;
import cz.fi.muni.pa165.dogbarbershop.service.AccountService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Baly
 */
@Controller
public class AccountController extends BaseController {

    @Autowired
    private AccountService accountService;
    
    @ModelAttribute("accounts")
    public List<AccountDTO> allAccounts(){
            return accountService.findAll();
    }

    @Secured ({"ROLE_ADMIN"})
    @RequestMapping(value = "/account/list", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("accounts", allAccounts());
        return "account/list";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
        public String loginPage(HttpServletRequest request) {
        String referer = request.getHeader("Referer");
        request.getSession().setAttribute("url_prior_login", referer);
        return "login";
    }
}
