/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.dogbarbershop.web.converters;

import cz.fi.muni.pa165.dogbarbershop.dto.PetDTO;
import cz.fi.muni.pa165.dogbarbershop.service.PetService;
import java.beans.PropertyEditorSupport;

/**
 *
 * @author Baly
 */
public class PetEditor extends PropertyEditorSupport {
    
    private PetService petService;
    
    public PetEditor(PetService service) {
        this.petService = service;
    }

    // Converts a String to a CustomerDTO (when submitting form)
    @Override
    public void setAsText(String text) {
        PetDTO c = petService.find(Long.parseLong(text));
        this.setValue(c);
    }
}
