/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.dogbarbershop.web.validation;

/**
 *
 * @author Petr Baláš
 */
public enum ValidationRegex {
    EMAIL("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"),
    PASSWORD("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})"),
    ONLY_LETTERS("\\p{L}+"),
    ONLY_POSITIVE_NUMBERS("^(?:[1-9]\\d*|0)?(?:\\.\\d+)?$");
    
    private String pattern;
    
    private ValidationRegex(String pattern) {
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }
}
