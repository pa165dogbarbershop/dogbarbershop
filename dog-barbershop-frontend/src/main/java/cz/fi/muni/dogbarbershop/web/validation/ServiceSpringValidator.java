package cz.fi.muni.dogbarbershop.web.validation;

import cz.fi.muni.pa165.dogbarbershop.dto.ServiceDTO;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ServiceSpringValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return ServiceDTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ServiceDTO service = (ServiceDTO) target;
                
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "required");
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required");
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "duration", "required");
                
                if(service.getPrice() > 60000) {
                    errors.rejectValue("price", "tooMuch");
                }
                
                if(service.getPrice() < 0) {
                    errors.rejectValue("price", "notEnough");
                }
                
                if(service.getName().length() > 40) {
                    errors.rejectValue("name", "tooLong");
                }
                
                if(service.getDuration() > 7200) {
                    errors.rejectValue("duration", "tooMuch");
                }
                
                if(service.getDuration() < 0) {
                    errors.rejectValue("duration", "notEnough");
                }
                
                if(service.getPet() == null) {
                    errors.rejectValue("pet", "mustHaveValue");
                }
	}

}
