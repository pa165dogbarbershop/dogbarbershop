package cz.fi.muni.dogbarbershop.web.ws;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cz.fi.muni.pa165.dogbarbershop.dto.PetDTO;
import cz.fi.muni.pa165.dogbarbershop.service.PetService;
import java.util.ArrayList;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * 
 * @author Jakub Knetl
 *
 */
@RestController
@RequestMapping("/ws/pets")
public class PetWebService {

	@Autowired
	private PetService petService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<PetDTO> getPet(@PathVariable("id") Long id) {
            HttpStatus status = HttpStatus.OK;
            
            PetDTO pet = null;
            try {
                pet = petService.find(id);
            } catch(Exception ex) {
                status = HttpStatus.NOT_FOUND;
            }
            
            return new ResponseEntity<PetDTO>(pet ,status);
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<PetDTO>> getAll(){
            HttpStatus status = HttpStatus.OK;
            
            List<PetDTO> pets = new ArrayList();
            
            try {
                pets = petService.findAll();
            } catch(Exception ex) {
                status = HttpStatus.INTERNAL_SERVER_ERROR;
            }
            
            return new ResponseEntity<List<PetDTO>>(pets, status);
	}

	@RequestMapping(value = "", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<PetDTO> add(@RequestBody PetDTO pet){
		
            HttpStatus status = HttpStatus.CREATED;
            
            try {
                petService.create(pet);
            } catch(Exception ex) {
                status = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            return new ResponseEntity<PetDTO>(pet,status);
	}
	
        @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, consumes = "application/json", produces = "application/json")
	public ResponseEntity<PetDTO> update(@RequestBody PetDTO pet){
            HttpStatus status = HttpStatus.OK;
            
            petService.update(pet);

            PetDTO p = petService.find(pet.getId());
            
            if(p == null) {
                status = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            return new ResponseEntity<PetDTO>(p,status);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
        @ResponseStatus(value = HttpStatus.OK)
	public void delete(@PathVariable("id") Long id) {
            PetDTO p = petService.find(id);
            petService.delete(p);
	}

}
