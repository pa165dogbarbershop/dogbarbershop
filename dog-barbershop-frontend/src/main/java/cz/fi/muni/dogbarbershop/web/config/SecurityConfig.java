package cz.fi.muni.dogbarbershop.web.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@ImportResource({ "classpath:jdbc-context.xml" })
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    static final Logger logger = LoggerFactory.getLogger(SecurityConfig.class);

    @Autowired
    private DataSource dataSource;

    @Bean
    public AuthenticationSuccessHandler successHandler() {
        return new SavedRequestAwareAuthenticationSuccessHandler() {

            @Override
            public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
                String redirectUrl = "/pet/list";
                boolean isAdmin = false;
                
                for (GrantedAuthority auth : authentication.getAuthorities()) {
                    if ("ROLE_ADMIN".equals(auth.getAuthority())) {
                        isAdmin = true;
                        redirectUrl = "/customer/list";
                        break;
                    }
                }
                
                String referer = (String) request.getSession().getAttribute("url_prior_login");
                if (referer != null) {
                    if (isAdmin || referer.matches("^http[s]?://.*/" + request.getContextPath().substring(1) + "/pet/.*")
                            || referer.matches("^http[s]?://.*/" + request.getContextPath().substring(1) + "/service/.*")) {
                        redirectUrl = referer;
                    }
                    request.getSession().removeAttribute("url_prior_login");
                }
                
                getRedirectStrategy().sendRedirect(request, response, redirectUrl);
            }

        };
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
            .antMatchers("/resources/**", "/ws/**").permitAll()
            .anyRequest().authenticated()
            .and()
            .formLogin().loginPage("/login").permitAll().successHandler(successHandler())
            .and()
            .logout().permitAll()
            .and()
            .csrf().disable();
    }


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        String selectUsersByUsername = "select login,password, enabled from account where login=?";
        String selectUsernameByQuery = "select login, role from account where login=?";

        auth.jdbcAuthentication().dataSource(dataSource).passwordEncoder(passwordEncoder())
                .usersByUsernameQuery(selectUsersByUsername)
                .authoritiesByUsernameQuery(selectUsernameByQuery).
            and()
                .inMemoryAuthentication()
                    .withUser("admin").password("admin").roles("ADMIN")
                .and()
                    .withUser("test").password("test").roles("USER");
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }
}
