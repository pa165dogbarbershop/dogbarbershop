package cz.fi.muni.dogbarbershop.web.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import cz.fi.muni.dogbarbershop.web.converters.CustomerEditor;
import cz.fi.muni.dogbarbershop.web.validation.CustomerSpringValidator;
import cz.fi.muni.dogbarbershop.web.validation.PetSpringValidator;
import cz.fi.muni.pa165.dogbarbershop.dto.AccountDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.CustomerDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.PetDTO;
import cz.fi.muni.pa165.dogbarbershop.entity.Pet.FurType;
import cz.fi.muni.pa165.dogbarbershop.service.AccountService;
import cz.fi.muni.pa165.dogbarbershop.service.CustomerService;
import cz.fi.muni.pa165.dogbarbershop.service.PetService;

/**
 * Customer controller.
 *
 * @author Petr Balas
 *
 */

@Controller
@RequestMapping("/pet")
public class PetController extends BaseController {

	@Autowired
	private PetService petService;

        @Autowired
	private CustomerService customerService;

        @Autowired
	private AccountService accountService;

        @Autowired
        private MessageSource messageSource;

	@ModelAttribute("pets")
	public List<PetDTO> allPets(){
		return petService.findAll();
	}

        @ModelAttribute("customersList")
	public List<CustomerDTO> allCustomers(){
            List<CustomerDTO> customers = customerService.findAll();

            AccountDTO loggedAccount = accountService.getLoggedAccount();
            CustomerDTO customer = null;
            if (loggedAccount != null){
                customer = customerService.personToCustomer(accountService.getLoggedAccount().getPerson());
            }

            if(!accountService.hasRole("ROLE_ADMIN") && customer != null) {
                customers = new ArrayList();
                customers.add(customer);
            }

            return customers;
	}

        @ModelAttribute("furTypes")
	public FurType[] allFurTypes(){
		return FurType.values();
	}

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String view(@PathVariable Long id, Model model) {
        model.addAttribute("pet", petService.find(id));
        return "pet/view";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        AccountDTO loggedAccount = accountService.getLoggedAccount();

        CustomerDTO customer = null;
        if (loggedAccount != null)
            customer = customerService.personToCustomer(accountService.getLoggedAccount().getPerson());

        if (!accountService.hasRole("ROLE_ADMIN") && customer != null) {
            model.addAttribute("pets", customer.getPets());
        } else {
            model.addAttribute("pets", allPets());
        }

        return "pet/list";
    }

    @RequestMapping(value = "/owner/{id}", method = RequestMethod.GET)
    public String pets(@PathVariable Long id, Model model) {
        model.addAttribute("petsCustomer", petService.find(id).getCustomer());
        return "customer/" + id;
    }

    @RequestMapping(value = "/{id}/services", method = RequestMethod.GET)
    public String petServices(@PathVariable Long id, Model model) {
        model.addAttribute("pet", petService.find(id));
        model.addAttribute("services", petService.find(id).getServices());
        return "pet/services";
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
    public String delete(@PathVariable Long id, RedirectAttributes redirectAttributes, Locale locale, UriComponentsBuilder uriBuilder) {
        PetDTO pet = petService.find(id);
        petService.delete(pet);
        redirectAttributes.addFlashAttribute(
                "message",
                messageSource.getMessage("petDTO.delete.message", new Object[]{pet.getName(), pet.getBreed(), pet.getId()}, locale)
        );
        return "redirect:" + uriBuilder.path("/pet/list").build();
    }

    @RequestMapping(value = "/{id}/update", method = RequestMethod.GET)
    public String update_form(@PathVariable Long id, Model model) {
        PetDTO pet = petService.find(id);
        model.addAttribute("pet", pet);

        return "pet/edit";
    }

    @RequestMapping(value="/add", method = RequestMethod.GET)
    public String add(Model model){
    	model.addAttribute("pet", new PetDTO());
    	return "pet/add";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("pet") PetDTO pet, BindingResult bindingResult, RedirectAttributes redirectAttributes, UriComponentsBuilder uriBuilder, Locale locale) {

        if (bindingResult.hasErrors()) {
            return (pet.getId()==null)?"pet/add":"pet/edit";
        }
        if (pet.getId() == null) {
            petService.create(pet);
            redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("petDTO.add.message", new Object[]{pet.getName(), pet.getBreed(), pet.getId()}, locale)
            );
        } else {
            petService.update(pet);
            redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("petDTO.updated.message", new Object[]{pet.getName(), pet.getBreed(), pet.getId()}, locale)
            );
        }
        return "redirect:" + uriBuilder.path("/pet/list").build();
    }

    @InitBinder("owner")
    protected void initCustomerBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));

        binder.registerCustomEditor(CustomerDTO.class, new CustomerEditor(customerService));
        binder.addValidators(new CustomerSpringValidator());
    }

    @InitBinder("pet")
    protected void initPetBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));

        binder.registerCustomEditor(CustomerDTO.class, new CustomerEditor(customerService));

        binder.addValidators(new PetSpringValidator());
    }

}
