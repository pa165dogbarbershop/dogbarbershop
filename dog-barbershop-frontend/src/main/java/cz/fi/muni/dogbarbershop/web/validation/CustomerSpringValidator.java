package cz.fi.muni.dogbarbershop.web.validation;

import cz.fi.muni.pa165.dogbarbershop.dto.CustomerDTO;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Petr Balas
 */
public class CustomerSpringValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return CustomerDTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        CustomerDTO customer = (CustomerDTO) target;
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customerNum", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.city", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.street", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.zip", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "account.login", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "account.password", "required");
        
        if (customer.getFirstName().length() > 30) {
            errors.rejectValue("firstName", "tooLong");
        }
        
        if (customer.getLastName().length() > 30) {
            errors.rejectValue("lastName", "tooLong");
        }

        if(!customer.getFirstName().matches(ValidationRegex.ONLY_LETTERS.getPattern())) {
            errors.rejectValue("firstName", "notOnlyLetters");
        }
        
        if(!customer.getLastName().matches(ValidationRegex.ONLY_LETTERS.getPattern())) {
            errors.rejectValue("lastName", "notOnlyLetters");
        }
        
        if(!customer.getEmail().matches(ValidationRegex.EMAIL.getPattern())) {
            errors.rejectValue("email", "wrongFormat");
        }
        
        if(customer.getEmail().length() > 70) {
            errors.rejectValue("email", "tooLong");
        }
        
        if(!customer.getPhone().matches(ValidationRegex.ONLY_POSITIVE_NUMBERS.getPattern())) {
            errors.rejectValue("phone", "wrongFormat");
        }
        
        if(customer.getPhone().length() < 9) {
            errors.rejectValue("phone", "tooShort");
        }

        if(customer.getAddress().getCity().length() > 40) {
            errors.rejectValue("address.city", "tooLong");
        }
        
        if(customer.getAddress().getStreet().length() > 40) {
            errors.rejectValue("address.street", "tooLong");
        }
        
        if(customer.getAddress().getZip().length() > 20) {
            errors.rejectValue("address.zip", "tooLong");
        }
        
        if(!customer.getAddress().getZip().matches(ValidationRegex.ONLY_POSITIVE_NUMBERS.getPattern())) {
            errors.rejectValue("address.zip", "wrongFormat");
        }
        
        if(customer.getCustomerNum() < 1 || customer.getCustomerNum() > Integer.MAX_VALUE) {
            errors.rejectValue("customerNum", "wrongRange");
        }
        
        if(!customer.getAccount().getPassword().matches(ValidationRegex.PASSWORD.getPattern())) {
            errors.rejectValue("account.password","wrongFormat");
        }
        
        if(customer.getAccount().getLogin().length() > 30) {
            errors.rejectValue("account.login","tooLong");
        }
    }
}
