/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fi.muni.dogbarbershop.web.ws;

import cz.fi.muni.pa165.dogbarbershop.dto.CustomerDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.PetDTO;
import cz.fi.muni.pa165.dogbarbershop.dto.ServiceDTO;
import cz.fi.muni.pa165.dogbarbershop.service.CustomerService;
import cz.fi.muni.pa165.dogbarbershop.service.PetService;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Petr Baláš
 */
@RestController
@RequestMapping("/ws/customers")
public class CustomerWebService {

	@Autowired
	private CustomerService customerService;
        
        @Autowired
	private PetService petService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<CustomerDTO> getCustomer(@PathVariable("id") Long id) {
            HttpStatus status = HttpStatus.OK;
            
            CustomerDTO customer = null;
            customer = customerService.find(id);
            if (customer == null) {
                status = HttpStatus.NOT_FOUND;
            }
            
            return new ResponseEntity<CustomerDTO>(customer, status);
	}
        
        @RequestMapping(value = "", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<CustomerDTO>> getAll(){
            HttpStatus status = HttpStatus.OK;
            
            List<CustomerDTO> customers = new ArrayList();
            
            try {
                customers = customerService.findAll();
            } catch(Exception ex) {
                status = HttpStatus.INTERNAL_SERVER_ERROR;
            }
            
            return new ResponseEntity<List<CustomerDTO>>(customers, status);
	}
        
        @RequestMapping(value = "/{id}/pets", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<Set<PetDTO>> getPets(@PathVariable("id") Long id) {
            HttpStatus status = HttpStatus.OK;
            
            Set<PetDTO> pets = null;
            try {
                CustomerDTO customer = customerService.find(id);
                pets = customer.getPets();
            } catch(Exception ex) {
                status = HttpStatus.NOT_FOUND;
            }
            
            return new ResponseEntity<Set<PetDTO>>(pets ,status);
	}
        
        @RequestMapping(value = "/{id}/services", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<ServiceDTO>> getServices(@PathVariable("id") Long id) {
            HttpStatus status = HttpStatus.OK;
            
            List<ServiceDTO> services = new ArrayList<>();
            try {
                CustomerDTO customer = customerService.find(id);
                for (PetDTO pet : customer.getPets()) {
                    services.addAll(pet.getServices());
                }
            } catch(Exception ex) {
                status = HttpStatus.NOT_FOUND;
            }
            
            return new ResponseEntity<>(services, status);
	}

	@RequestMapping(value = "", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<CustomerDTO> add(@RequestBody CustomerDTO customer){
            
            HttpStatus status = HttpStatus.CREATED;
            
            try {
                customerService.create(customer);
            } catch(Exception ex) {
                status = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            return new ResponseEntity<CustomerDTO>(customer,status);
	}
	
        @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, consumes = "application/json", produces = "application/json")
	public ResponseEntity<CustomerDTO> update(@RequestBody CustomerDTO customer){
            HttpStatus status = HttpStatus.OK;
            
            customerService.update(customer);

            CustomerDTO c = customerService.find(customer.getId());
            
            if(c == null) {
                status = HttpStatus.INTERNAL_SERVER_ERROR;
            }

            return new ResponseEntity<CustomerDTO>(c,status);
	}
        
        @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
        @ResponseStatus(value = HttpStatus.OK)
	public void delete(@PathVariable("id") Long id){
            CustomerDTO customer = customerService.find(id);
            customerService.delete(customer);
	}
}
