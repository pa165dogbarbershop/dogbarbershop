package cz.fi.muni.dogbarbershop.web.controller;

import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import cz.fi.muni.dogbarbershop.web.validation.EmployeeSpringValidator;
import cz.fi.muni.pa165.dogbarbershop.dto.EmployeeDTO;
import cz.fi.muni.pa165.dogbarbershop.service.EmployeeService;
import org.springframework.security.access.annotation.Secured;

@Controller
@Secured ({"ROLE_ADMIN", "ROLE_EMPLOYEE"})
@RequestMapping("/employee")
public class EmployeeController extends BaseController {

	@Autowired
	private EmployeeService employeeService;
        
	@Autowired
	private MessageSource messageSource;
	
	@ModelAttribute("employee/list")
	public List<EmployeeDTO> allEmployees(){
		return employeeService.findAll();
	}
        
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String view(@PathVariable Long id, Model model) {
        model.addAttribute("employee", employeeService.find(id));
        return "employee/view";
    }
	
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("employees", allEmployees());
        return "employee/list";
    }
    
   
    @RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
    public String delete(@PathVariable Long id, RedirectAttributes redirectAttributes, Locale locale, UriComponentsBuilder uriBuilder) {
        EmployeeDTO employee = employeeService.find(id);;
        employeeService.delete(employee);
        redirectAttributes.addFlashAttribute(
                "message",
                messageSource.getMessage("employeeDTO.delete.message", new Object[]{employee.getFirstName(), employee.getLastName(), employee.getId()}, locale)
        );
        return "redirect:" + uriBuilder.path("/employee/list").build();
    }

    @RequestMapping(value = "/{id}/update", method = RequestMethod.GET)
    public String update_form(@PathVariable Long id, Model model) {
        EmployeeDTO employee = employeeService.find(id);
        model.addAttribute("employee", employee);

        return "employee/edit";
    }
    
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(new EmployeeSpringValidator());
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("employee") EmployeeDTO employee, BindingResult bindingResult, RedirectAttributes redirectAttributes, UriComponentsBuilder uriBuilder, Locale locale) {

        if (bindingResult.hasErrors()) {
            return (employee.getId() == null)?"employee/add":"employee/edit";
        }
        if (employee.getId() == null) {
            employeeService.create(employee);
            redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("employeeDTO.add.message", new Object[]{employee.getFirstName(), employee.getLastName(), employee.getId()}, locale)
            );
        } else {
            employeeService.update(employee);
            redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("employeeDTO.updated.message", new Object[]{employee.getFirstName(), employee.getLastName(), employee.getId()}, locale)
            );
        }
        return "redirect:" + uriBuilder.path("/employee/list").build();
    }
    
    @RequestMapping(value="/add", method = RequestMethod.GET)
    public String add(Model model){
    	model.addAttribute("employee", new EmployeeDTO());
    	return "employee/add";
    }
}
