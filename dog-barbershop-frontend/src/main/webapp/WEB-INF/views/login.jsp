<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no">
        
        <title><fmt:message key="login.title" /> – <fmt:message key="siteTitle" /></title>
        
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <form:form method="POST" action="${pageContext.request.contextPath}/login" class="form-signin">
                <c:if test="${param.error != null}">
                    <div class="alert alert-danger" role="alert">
                        <fmt:message key="login.error" />
                    </div>
                </c:if>
                <c:if test="${param.logout != null}">
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"><fmt:message key="nav.close" /></span></button>
                        <fmt:message key="logout.success" />
                    </div>
                </c:if>
                <h2 class="form-signin-heading"><fmt:message key="login.intro" /></h2>
                <label for="username" class="sr-only"><fmt:message key="login.username" /></label>
                <input type="text" id="username" name="username" class="form-control" placeholder="<fmt:message key="login.username" />" required="" autofocus="">
                <label for="password" class="sr-only"><fmt:message key="login.password" /></label>
                <input type="password" id="password" name="password" class="form-control" placeholder="<fmt:message key="login.password" />" required="">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <button class="btn btn-lg btn-primary btn-block" type="submit"><fmt:message key="login.verb" /></button>
            </form:form>
        </div>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
        <script src="<c:url value="/resources/js/application.js" />"></script>
    </body>
</html>
