<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="pet" />
<t:layout pageTitle="${pet.name} – ${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1><fmt:message key="pet" /> <small>${pet.name}</small></h1>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <div class="pull-left">
                <a href="<c:url value="/pet/${pet.id}/services" />" class="btn btn-primary">
                    <span><fmt:message key="pet.viewServices" /></span>
                </a>
            </div>
            <div class="pull-right">
                <a href="<c:url value="/pet/${pet.id}/update" />" class="btn btn-warning">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    <span class="hidden-xs"> <fmt:message key="pet.edit" /></span>
                </a>
                <form method="post" action="${pageContext.request.contextPath}/pet/${pet.id}/delete" class="delete">
                    <button type="submit" class="btn btn-danger">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        <span class="hidden-xs"> <fmt:message key="pet.delete" /></span>
                    </button>
                </form>
            </div>
        </div>
        <div class="panel-body">
            <dl class="dl-horizontal">
                <dt><fmt:message key="pet.id" /></dt>
                <dd>${pet.id}</dd>
                <dt><fmt:message key="pet.name" /></dt>
                <dd>${pet.name}</dd>
                <dt><fmt:message key="pet.breed" /></dt>
                <dd>${pet.breed}</dd>
                <dt><fmt:message key="pet.birthDate" /></dt>
                <dd>${pet.birthDate}</dd>
                <dt><fmt:message key="pet.birthDate" /></dt>
                <dd>${pet.fur}</dd>
                <dt><fmt:message key="pet.owner" /></dt>
                <dd><a href="<c:url value="/customer/${pet.customer.id}" />">${pet.customer.fullName}</a></dd>
            </dl>
        </div>
    </div>
</t:layout>
