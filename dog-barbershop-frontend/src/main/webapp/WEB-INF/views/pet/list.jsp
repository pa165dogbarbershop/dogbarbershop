<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="pets" />
<t:layout pageTitle="${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1>${pageTitle}</h1>
        </div>
        <div class="btn-action pull-right">
            <a href="<c:url value="/pet/add" />" class="btn btn-success">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                <span class="hidden-xs"> <fmt:message key="pet.newPet" /></span>
            </a>
        </div>
    </div>
    
    <div>
        <table class="content-table table table-striped">
            <thead class="hidden-xs">
                <tr>
                    <th class="hidden-xs"><fmt:message key="pet.id" /></th>
                    <th><fmt:message key="pet.name" /></th>
                    <th class="hidden-xs hidden-sm"><fmt:message key="pet.breed" /></th>
                    <th class="visible-lg"><fmt:message key="pet.birthDate" /></th>
                    <th class="visible-lg"><fmt:message key="pet.fur" /></th>
                    <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')"><th class="visible-lg"><fmt:message key="pet.owner" /></th></sec:authorize>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${pets}" var="pet">
                    <tr>
                        <td class="hidden-xs">${pet.id}</td>
                        <td>
                            <a href="<c:url value="/pet/${pet.id}" />">${pet.name}</a>
                        </td>
                        <td class="hidden-xs hidden-sm">${pet.breed}</td>
                        <td class="visible-lg">${pet.birthDate}</td>
                        <td class="visible-lg">${pet.fur}</td>
                        <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')"><td class="visible-lg">
                            <a href="<c:url value="/customer/${pet.customer.id}" />">${pet.customer.firstName} ${pet.customer.lastName}</a>
                        </td></sec:authorize>
                        <td class="text-right">
                            <div class="hidden-xs">
                                <a href="<c:url value="/pet/${pet.id}/services" />" class="btn btn-primary"><fmt:message key="pet.viewServices" /></a>
                                <a href="<c:url value="/pet/${pet.id}/update" />" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                                <form method="post" action="${pageContext.request.contextPath}/pet/${pet.id}/delete" class="delete">
                                    <button type="submit" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </form>
                            </div>
                            <div class="dropdown visible-xs">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                    <fmt:message key="pet.actions" /> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a href="<c:url value="/pet/${pet.id}/services" />"><fmt:message key="pet.viewServices" /></a></li>
                                    <li><a href="<c:url value="/pet/${pet.id}/update" />"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <fmt:message key="pet.edit" /></a></li>
                                    <li><form method="post" action="${pageContext.request.contextPath}/pet/${pet.id}/delete" class="delete"><button type="submit" class="btn btn-link"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <fmt:message key="pet.delete" /></button></form></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</t:layout>
