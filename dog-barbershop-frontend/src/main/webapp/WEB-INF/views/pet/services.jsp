<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="services" />
<t:layout pageTitle="${pet.name}'s ${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1>${pet.name} <small>${pageTitle}</small></h1>
        </div>
    </div>
    
    <div>
        <table class="content-table table table-striped">
            <thead class="hidden-xs">
                <tr>
                    <th class="hidden-xs"><fmt:message key="service.id" /></th>
                    <th><fmt:message key="service.name" /></th>
                    <th class="hidden-xs hidden-sm"><fmt:message key="service.duration" /></th>
                    <th class="hidden-xs hidden-sm"><fmt:message key="service.price" /></th>
                    <th class="visible-lg"><fmt:message key="service.pet" /></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${services}" var="service">
                    <tr>
                        <td class="hidden-xs">${service.id}</td>
                        <td>
                            <a href="<c:url value="/service/${service.id}" />">${service.name}</a>
                        </td>
                        <td class="hidden-xs hidden-sm">${service.duration}</td>
                        <td class="hidden-xs hidden-sm">${service.price}</td>
                        <td class="visible-lg">
                            <a href="<c:url value="/pet/${service.pet.id}" />">${service.pet.name}</a>
                        </td>
                        <td class="text-right">
                            <div class="hidden-xs">
                                <a href="<c:url value="/service/${service.id}/update" />" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                                <form method="post" action="${pageContext.request.contextPath}/service/${service.id}/delete" class="delete">
                                    <button type="submit" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </form>
                            </div>
                            <div class="dropdown visible-xs">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                    <fmt:message key="service.actions" /> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a href="<c:url value="/service/${service.id}/update" />"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <fmt:message key="service.edit" /></a></li>
                                    <li><form method="post" action="${pageContext.request.contextPath}/service/${service.id}/delete" class="delete"><button type="submit" class="btn btn-link"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <fmt:message key="service.delete" /></button></form></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</t:layout>
