<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="pet.editPet" />
<t:layout pageTitle="${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1>${pageTitle} <small>${pet.name}</small></h1>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <form:form method="post" action="${pageContext.request.contextPath}/pet/update"
                       modelAttribute="pet" class="form-horizontal" role="form">
                <div style="display: none">
                    <div class="col-sm-10">
                        <form:input path="id" class="form-control" cssErrorClass="form-control error" placeholder="${id}" />
                        <form:errors path="id" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="name" key="pet.name" />
                    <form:label path="name" class="col-sm-2 control-label">${name}</form:label>
                    <div class="col-sm-10">
                        <form:input path="name" class="form-control" cssErrorClass="form-control error" placeholder="${name}" />
                        <form:errors path="name" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="breed" key="pet.breed" />
                    <form:label path="breed" class="col-sm-2 control-label">${breed}</form:label>
                    <div class="col-sm-10">
                        <form:input path="breed" class="form-control" cssErrorClass="form-control error" placeholder="${breed}" />
                        <form:errors path="breed" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="birthDate" key="pet.birthDate" />
                    <form:label path="birthDate" class="col-sm-2 control-label">${birthDate}</form:label>
                    <div class="col-sm-10">
                        <form:input path="birthDate" class="form-control" cssErrorClass="form-control error" placeholder="${birthDate}" />
                        <form:errors path="birthDate" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="fur" key="pet.fur" />
                    <form:label path="fur" class="col-sm-2 control-label">${fur}</form:label>
                    <div class="col-sm-10">
                        <form:select path="fur" class="form-control" cssErrorClass="form-control error" >
                            <form:options items="${furTypes}" />
                        </form:select>
                        <form:errors path="fur" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="owner" key="pet.owner" />
                    <form:label path="customer" class="col-sm-2 control-label">${owner}</form:label>
                    <div class="col-sm-10">
                        <form:select path="customer" class="form-control" cssErrorClass="form-control error">
                            <form:options items="${customersList}" itemLabel="fullName" itemValue="id"/>
                        </form:select>
                        <form:errors path="customer" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <fmt:message key="pet.save" />
                        </button>
                        <a href="<c:url value="/pet/${pet.id}" />" class="btn btn-default" role="button"><fmt:message key="pet.cancel" /></a>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</t:layout>
