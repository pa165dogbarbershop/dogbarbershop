<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="customer.editCustomer" />
<t:layout pageTitle="${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1>${pageTitle} <small>${customer.firstName} ${customer.lastName}</small></h1>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <form:form method="post" action="${pageContext.request.contextPath}/customer/update"
                       modelAttribute="customer" class="form-horizontal" role="form">
                <div style="display: none">
                    <div class="col-sm-10">
                        <form:input path="id" class="form-control" cssErrorClass="form-control error" placeholder="${id}" />
                        <form:errors path="id" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="firstName" key="customer.firstName" />
                    <form:label path="firstName" class="col-sm-2 control-label">${firstName}</form:label>
                    <div class="col-sm-10">
                        <form:input path="firstName" class="form-control" cssErrorClass="form-control error" placeholder="${firstName}" />
                        <form:errors path="firstName" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="lastName" key="customer.lastName" />
                    <form:label path="lastName" class="col-sm-2 control-label">${lastName}</form:label>
                    <div class="col-sm-10">
                        <form:input path="lastName" class="form-control" cssErrorClass="form-control error" placeholder="${lastName}" />
                        <form:errors path="lastName" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="email" key="customer.email" />
                    <form:label path="email" class="col-sm-2 control-label">${email}</form:label>
                    <div class="col-sm-10">
                        <form:input path="email" class="form-control" cssErrorClass="form-control error" placeholder="${email}" />
                        <form:errors path="email" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="login" key="account.login" />
                    <form:label path="account.login" class="col-sm-2 control-label">${login}</form:label>
                    <div class="col-sm-10">
                        <form:input path="account.login" class="form-control" cssErrorClass="form-control error" placeholder="${login}" />
                        <form:errors path="account.login" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="password" key="account.password" />
                    <form:label path="account.password" class="col-sm-2 control-label">${password}</form:label>
                    <div class="col-sm-10">
                        <form:password path="account.password" class="form-control" cssErrorClass="form-control error" placeholder="${password}" />
                        <form:errors path="account.password" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="street" key="customer.address.street" />
                    <form:label path="address.street" class="col-sm-2 control-label">${street}</form:label>
                    <div class="col-sm-10">
                        <form:input path="address.street" class="form-control" cssErrorClass="form-control error" placeholder="${street}" />
                        <form:errors path="address.street" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="city" key="customer.address.city" />
                    <form:label path="address.city" class="col-sm-2 control-label">${city}</form:label>
                    <div class="col-sm-10">
                        <form:input path="address.city" class="form-control" cssErrorClass="form-control error" placeholder="${city}" />
                        <form:errors path="address.city" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="zip" key="customer.address.zip" />
                    <form:label path="address.zip" class="col-sm-2 control-label">${zip}</form:label>
                    <div class="col-sm-10">
                        <form:input path="address.zip" class="form-control" cssErrorClass="form-control error" placeholder="${zip}" />
                        <form:errors path="address.zip" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="phone" key="customer.phone" />
                    <form:label path="phone" class="col-sm-2 control-label">${phone}</form:label>
                    <div class="col-sm-10">
                        <form:input path="phone" class="form-control" cssErrorClass="form-control error" placeholder="${phone}" />
                        <form:errors path="phone" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="customerNum" key="customer.customerNum" />
                    <form:label path="customerNum" class="col-sm-2 control-label">${customerNum}</form:label>
                    <div class="col-sm-10">
                        <form:input path="customerNum" class="form-control" cssErrorClass="form-control error" placeholder="${customerNum}" />
                        <form:errors path="customerNum" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <form:hidden path="account.id" />
                        <form:hidden path="account.role" value="USER" />
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <fmt:message key="customer.save" />
                        </button>
                        <a href="<c:url value="/customer/${customer.id}" />" class="btn btn-default" role="button"><fmt:message key="customer.cancel" /></a>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</t:layout>
