<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="customers" />
<t:layout pageTitle="${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1>${pageTitle}</h1>
        </div>
        <div class="btn-action pull-right">
            <a href="<c:url value="/customer/add" />" class="btn btn-success">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                <span class="hidden-xs"> <fmt:message key="customer.newCustomer" /></span>
            </a>
        </div>
    </div>
    
    <div>
        <table class="content-table table table-striped">
            <thead class="hidden-xs">
                <tr>
                    <th class="hidden-xs"><fmt:message key="customer.id" /></th>
                    <th><fmt:message key="customer.name" /></th>
                    <th class="hidden-xs hidden-sm"><fmt:message key="customer.email" /></th>
                    <th class="visible-lg"><fmt:message key="customer.address" /></th>
                    <th class="visible-lg"><fmt:message key="customer.phone" /></th>
                    <th class="hidden-xs"><fmt:message key="customer.customerNum" /></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${customers}" var="customer">
                    <tr>
                        <td class="hidden-xs">${customer.id}</td>
                        <td>
                            <a href="<c:url value="/customer/${customer.id}" />">${customer.firstName} ${customer.lastName}</a>
                        </td>
                        <td class="hidden-xs hidden-sm">
                            <a href="mailto:${customer.email}">${customer.email}</a>
                        </td>
                        <td class="visible-lg">
                            <address>${customer.address.street}<br>${customer.address.zip} ${customer.address.city}</address>
                        </td>
                        <td class="visible-lg">
                            <address>${customer.phone}</address>
                        </td>
                        <td class="hidden-xs">${customer.customerNum}</td>
                        <td class="text-right">
                            <div class="hidden-xs">
                                <a href="<c:url value="/customer/${customer.id}/pets" />" class="btn btn-primary"><fmt:message key="customer.viewPets" /></a>
                                <a href="<c:url value="/customer/${customer.id}/update" />" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                                <form method="post" action="${pageContext.request.contextPath}/customer/${customer.id}/delete" class="delete">
                                    <button type="submit" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </form>
                            </div>
                            <div class="dropdown visible-xs">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                    <fmt:message key="customer.actions" /> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a href="<c:url value="/customer/${customer.id}/pets" />"><fmt:message key="customer.viewPets" /></a></li>
                                    <li><a href="<c:url value="/customer/${customer.id}/update" />"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <fmt:message key="customer.edit" /></a></li>
                                    <li><form method="post" action="${pageContext.request.contextPath}/customer/${customer.id}/delete" class="delete"><button type="submit" class="btn btn-link"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <fmt:message key="customer.delete" /></button></form></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</t:layout>
