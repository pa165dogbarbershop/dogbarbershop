<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="pets" />
<t:layout pageTitle="${customer.firstName} ${customer.lastName}'s ${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1>${customer.firstName} ${customer.lastName} <small>${pageTitle}</small></h1>
        </div>
    </div>
    
    <div>
        <table class="content-table table table-striped">
            <thead class="hidden-xs">
                <tr>
                    <th class="hidden-xs"><fmt:message key="pet.id" /></th>
                    <th><fmt:message key="pet.name" /></th>
                    <th class="hidden-xs hidden-sm"><fmt:message key="pet.breed" /></th>
                    <th class="visible-lg"><fmt:message key="pet.birthDate" /></th>
                    <th class="visible-lg"><fmt:message key="pet.fur" /></th>
                    <th class="visible-lg"><fmt:message key="pet.owner" /></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${customerPets}" var="pet">
                    <tr>
                        <td class="hidden-xs">${pet.id}</td>
                        <td>
                            <a href="<c:url value="/pet/${pet.id}" />">${pet.name}</a>
                        </td>
                        <td class="hidden-xs hidden-sm">${pet.breed}</td>
                        <td class="visible-lg">${pet.birthDate}</td>
                        <td class="visible-lg">${pet.fur}</td>
                        <td class="visible-lg">
                            <a href="<c:url value="/customer/${pet.customer.id}" />">${pet.customer.firstName} ${pet.customer.lastName}</a>
                        </td>
                        <td class="text-right">
                            <div class="hidden-xs">
                                <a href="<c:url value="/pet/${pet.id}/services" />" class="btn btn-primary"><fmt:message key="pet.viewServices" /></a>
                                <a href="<c:url value="/pet/${pet.id}/update" />" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                                <a href="<c:url value="/pet/${pet.id}/delete" />" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </a>
                            </div>
                            <div class="dropdown visible-xs">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                    <fmt:message key="pet.actions" /> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a href="<c:url value="/pet/${pet.id}/services" />"><fmt:message key="pet.viewServices" /></a></li>
                                    <li><a href="<c:url value="/pet/${customer.id}/update" />"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <fmt:message key="pet.edit" /></a></li>
                                    <li><a href="<c:url value="/pet/${customer.id}/delete" />"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <fmt:message key="pet.delete" /></a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</t:layout>
