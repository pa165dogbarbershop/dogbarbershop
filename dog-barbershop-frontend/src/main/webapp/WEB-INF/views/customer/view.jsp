<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="customer" />
<t:layout pageTitle="${customer.firstName} ${customer.lastName} – ${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1><fmt:message key="customer" /> <small>${customer.firstName} ${customer.lastName}</small></h1>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <div class="pull-left">
                <a href="<c:url value="/customer/${customer.id}/pets" />" class="btn btn-primary">
                    <span><fmt:message key="customer.viewPets" /></span>
                </a>
            </div>
            <div class="pull-right">
                <a href="<c:url value="/customer/${customer.id}/update" />" class="btn btn-warning">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    <span class="hidden-xs"> <fmt:message key="customer.edit" /></span>
                </a>
                <form method="post" action="${pageContext.request.contextPath}/customer/${customer.id}/delete" class="delete">
                    <button type="submit" class="btn btn-danger">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        <span class="hidden-xs"> <fmt:message key="customer.delete" /></span>
                    </button>
                </form>
            </div>
        </div>
        <div class="panel-body">
            <dl class="dl-horizontal">
                <dt><fmt:message key="customer.id" /></dt>
                <dd>${customer.id}</dd>
                <dt><fmt:message key="customer.firstName" /></dt>
                <dd>${customer.firstName}</dd>
                <dt><fmt:message key="customer.lastName" /></dt>
                <dd>${customer.lastName}</dd>
                <dt><fmt:message key="customer.email" /></dt>
                <dd><a href="mailto:${customer.email}">${customer.email}</a></dd>
                <dt><fmt:message key="customer.address" /></dt>
                <dd><address>${customer.address.street}<br>${customer.address.zip} ${customer.address.city}</address></dd>
                <dt><fmt:message key="customer.phone" /></dt>
                <dd>${customer.phone}</dd>
                <dt><fmt:message key="customer.customerNum" /></dt>
                <dd>${customer.customerNum}</dd>
            </dl>
        </div>
    </div>
</t:layout>
