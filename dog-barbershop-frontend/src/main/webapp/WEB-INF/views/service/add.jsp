<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="service.createNew" />
<t:layout pageTitle="${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1>${pageTitle}</h1>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <form:form method="post" action="${pageContext.request.contextPath}/service/update"
                       modelAttribute="service" class="form-horizontal" role="form">
                <div style="display: none">
                    <div class="col-sm-10">
                        <form:input path="id" class="form-control" cssErrorClass="form-control error" placeholder="${id}" />
                        <form:errors path="id" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="name" key="service.name" />
                    <form:label path="name" class="col-sm-2 control-label">${name}</form:label>
                    <div class="col-sm-10">
                        <form:input path="name" class="form-control" cssErrorClass="form-control error" placeholder="${name}" />
                        <form:errors path="name" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="duration" key="service.duration" />
                    <form:label path="duration" class="col-sm-2 control-label">${duration}</form:label>
                    <div class="col-sm-10">
                        <form:input path="duration" class="form-control" cssErrorClass="form-control error" placeholder="${duration}" />
                        <form:errors path="duration" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="price" key="service.price" />
                    <form:label path="price" class="col-sm-2 control-label">${price}</form:label>
                    <div class="col-sm-10">
                        <form:input path="price" class="form-control" cssErrorClass="form-control error" placeholder="${price}" />
                        <form:errors path="price" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <fmt:message var="pet" key="service.pet" />
                    <form:label path="pet" class="col-sm-2 control-label">${pet}</form:label>
                    <div class="col-sm-10">
                        <form:select path="pet" class="form-control" cssErrorClass="form-control error" >
                            <form:options items="${petsList}" itemLabel="fullName" itemValue="id"/>
                        </form:select>
                        <form:errors path="pet" cssClass="danger-block" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <fmt:message key="service.create" />
                        </button>
                        <a href="<c:url value="/service/list" />" class="btn btn-default" role="button"><fmt:message key="service.cancel" /></a>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</t:layout>
