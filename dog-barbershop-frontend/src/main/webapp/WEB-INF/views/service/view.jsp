<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="service" />
<t:layout pageTitle="${service.name} – ${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1><fmt:message key="service" /> <small>${service.name}</small></h1>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <div class="pull-right">
                <a href="<c:url value="/service/${service.id}/update" />" class="btn btn-warning">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    <span class="hidden-xs"> <fmt:message key="service.edit" /></span>
                </a>
                <form method="post" action="${pageContext.request.contextPath}/service/${service.id}/delete" class="delete">
                    <button type="submit" class="btn btn-danger">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        <span class="hidden-xs"> <fmt:message key="service.delete" /></span>
                    </button>
                </form>
            </div>
        </div>
        <div class="panel-body">
            <dl class="dl-horizontal">
                <dt><fmt:message key="service.id" /></dt>
                <dd>${service.id}</dd>
                <dt><fmt:message key="service.name" /></dt>
                <dd>${service.name}</dd>
                <dt><fmt:message key="service.duration" /></dt>
                <dd>${service.duration}</dd>
                <dt><fmt:message key="service.price" /></dt>
                <dd>${service.price}</dd>
                <dt><fmt:message key="service.pet" /></dt>
                <dd><a href="<c:url value="/pet/${service.pet.id}" />">${service.pet.name}</a></dd>
            </dl>
        </div>
    </div>
</t:layout>
