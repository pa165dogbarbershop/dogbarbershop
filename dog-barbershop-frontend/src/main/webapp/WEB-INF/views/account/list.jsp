<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="accounts" />
<t:layout pageTitle="${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1>${pageTitle}</h1>
        </div>
    </div>
    
    <div>
        <table class="content-table table table-striped">
            <thead class="hidden-xs">
                <tr>
                    <th><fmt:message key="account.id" /></th>
                    <th><fmt:message key="account.name" /></th>
                    <th><fmt:message key="account.login" /></th>
                    <th><fmt:message key="account.role" /></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${accounts}" var="account">
                    <tr>
                        <td>${account.id}</td>
                        <td>${account.person.firstName} ${account.person.lastName}</td>
                        <td>${account.login}</td>
                        <td>${account.role}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</t:layout>
