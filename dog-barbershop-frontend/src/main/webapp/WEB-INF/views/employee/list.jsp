<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="employees" />
<t:layout pageTitle="${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1>${pageTitle}</h1>
        </div>
        <div class="btn-action pull-right">
            <a href="<c:url value="/employee/add" />" class="btn btn-success">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                <span class="hidden-xs"> <fmt:message key="employee.newEmployee" /></span>
            </a>
        </div>
    </div>
    
    <div>
        <table class="content-table table table-striped">
            <thead class="hidden-xs">
                <tr>
                    <th class="hidden-xs"><fmt:message key="employee.id" /></th>
                    <th><fmt:message key="employee.name" /></th>
                    <th class="hidden-xs hidden-sm"><fmt:message key="employee.email" /></th>
                    <th class="visible-lg"><fmt:message key="employee.address" /></th>
                    <th class="visible-lg"><fmt:message key="employee.phone" /></th>
                    <th class="hidden-xs"><fmt:message key="employee.salary" /></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${employees}" var="employee">
                    <tr>
                        <td class="hidden-xs">${employee.id}</td>
                        <td>
                            <a href="<c:url value="/employee/${employee.id}" />">${employee.firstName} ${employee.lastName}</a>
                        </td>
                        <td class="hidden-xs hidden-sm">
                            <a href="mailto:${employee.email}">${employee.email}</a>
                        </td>
                        <td class="visible-lg">
                            <address>${employee.address.street}<br>${employee.address.zip} ${employee.address.city}</address>
                        </td>
                        <td class="visible-lg">
                            <address>${employee.phone}</address>
                        </td>
                        <td class="hidden-xs">${employee.salary}</td>
                        <td class="text-right">
                            <div class="hidden-xs">
                                <a href="<c:url value="/employee/${employee.id}/update" />" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                                <form method="post" action="${pageContext.request.contextPath}/employee/${employee.id}/delete" class="delete">
                                    <button type="submit" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </form>
                            </div>
                            <div class="dropdown visible-xs">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                    <fmt:message key="employee.actions" /> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a href="<c:url value="/employee/${employee.id}/update" />"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <fmt:message key="employee.edit" /></a></li>
                                    <li><form method="post" action="${pageContext.request.contextPath}/employee/${employee.id}/delete" class="delete"><button type="submit" class="btn btn-link"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <fmt:message key="employee.delete" /></button></form></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</t:layout>
