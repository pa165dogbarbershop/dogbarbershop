<%@ page session="false" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:message var="pageTitle" key="employee" />
<t:layout pageTitle="${employee.firstName} ${employee.lastName} – ${pageTitle}">
    <div class="clearfix">
        <div class="pull-left">
            <h1><fmt:message key="employee" /> <small>${employee.firstName} ${employee.lastName}</small></h1>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <div class="pull-right">
                <a href="<c:url value="/employee/${employee.id}/update" />" class="btn btn-warning">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    <span class="hidden-xs"> <fmt:message key="employee.edit" /></span>
                </a>
                <form method="post" action="${pageContext.request.contextPath}/employee/${employee.id}/delete" class="delete">
                    <button type="submit" class="btn btn-danger">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        <span class="hidden-xs"> <fmt:message key="employee.delete" /></span>
                    </button>
                </form>
            </div>
        </div>
        <div class="panel-body">
            <dl class="dl-horizontal">
                <dt><fmt:message key="employee.id" /></dt>
                <dd>${employee.id}</dd>
                <dt><fmt:message key="employee.firstName" /></dt>
                <dd>${employee.firstName}</dd>
                <dt><fmt:message key="employee.lastName" /></dt>
                <dd>${employee.lastName}</dd>
                <dt><fmt:message key="employee.email" /></dt>
                <dd><a href="mailto:${employee.email}">${employee.email}</a></dd>
                <dt><fmt:message key="employee.address" /></dt>
                <dd><address>${employee.address.street}<br>${employee.address.zip} ${employee.address.city}</address></dd>
                <dt><fmt:message key="employee.phone" /></dt>
                <dd>${employee.phone}</dd>
                <dt><fmt:message key="employee.salary" /></dt>
                <dd>${employee.salary}</dd>
            </dl>
        </div>
    </div>
</t:layout>
