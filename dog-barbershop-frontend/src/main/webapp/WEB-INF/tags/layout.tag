<%@ tag description="Basic Layout" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ attribute name="pageTitle" required="true" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no">
        
        <title>${pageTitle} – <fmt:message key="siteTitle" /></title>
        
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only"><fmt:message key="nav.toggle" /></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<c:url value="/customer/list" />"><fmt:message key="siteTitle" /></a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li<c:if test="${controllerName eq 'account'}"> class="active"</c:if>><a href="<c:url value="/account/list" />"><fmt:message key="nav.accounts" /></a></li>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')">
                        <li<c:if test="${controllerName eq 'customer'}"> class="active"</c:if>><a href="<c:url value="/customer/list" />"><fmt:message key="nav.customers" /></a></li>
                        <li<c:if test="${controllerName eq 'employee'}"> class="active"</c:if>><a href="<c:url value="/employee/list" />"><fmt:message key="nav.employees" /></a></li>
                        </sec:authorize>
                        <li<c:if test="${controllerName eq 'pet'}"> class="active"</c:if>><a href="<c:url value="/pet/list" />"><fmt:message key="nav.pets" /></a></li>
                        <li<c:if test="${controllerName eq 'service'}"> class="active"</c:if>><a href="<c:url value="/service/list" />"><fmt:message key="nav.services" /></a></li>
                    </ul>
                    <p class="navbar-text navbar-right">
                        <fmt:message key="login.loggedinas" /> <strong><sec:authentication property="principal.username" /></strong>. <a href="${pageContext.request.contextPath}/logout" class="navbar-link"><fmt:message key="logout.verb" /></a>
                    </p>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="main">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"><fmt:message key="nav.close" /></span></button>
                        ${message}
                    </div>
                </c:if>
                
                <jsp:doBody/>
            </div>
        </div>
            
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
        <script src="<c:url value="/resources/js/application.js" />"></script>
    </body>
</html>
